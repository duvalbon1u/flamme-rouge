/**
 * Exception permettant de reporter une valeur incorrecte.
 * @author Adrien
 *
 */
@SuppressWarnings("serial") //On n'utilise pas la sérialisation ici
public class ValeurIncorrecteException extends Exception {
	/**
	 * Constructeur par défaut
	 */
	public ValeurIncorrecteException() {
		super();
	}
	
	/**
	 * Construit l'exception avec un message.
	 * @param m Message de l'erreur.
	 */
	public ValeurIncorrecteException(String m)
	{
		super(m);
	}
}
