import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import libtest.*;

/**
 * Une classe de test sur les Etapes.
 * @author Adrien
 * @author Arthur
 *
 */
public class TestEtape {

	public static void test_a_getTuileType()
	{

		try{
			List<Tuile> tuiles = new ArrayList<Tuile>();
			tuiles.add(new Tuile("1n"));
			tuiles.add(new Tuile("1d1m1s"));
			tuiles.add(new Tuile("1n1a"));
			Etape e = new Etape(tuiles);

			OutilTest.assertEquals("La tuile num 1 est incorrecte", Tuile.NORMAL, e.getTuileType(0));
			OutilTest.assertEquals("La tuile num 2 est incorrecte", Tuile.DESCENTE, e.getTuileType(1));
			OutilTest.assertEquals("La tuile num 3 est incorrecte", Tuile.MONTE, e.getTuileType(2));
			OutilTest.assertEquals("La tuile num 4 est incorrecte", Tuile.DEPART, e.getTuileType(3));
			OutilTest.assertEquals("La tuile num 5 est incorrecte", Tuile.NORMAL, e.getTuileType(4));
			OutilTest.assertEquals("La tuile num 6 est incorrecte", Tuile.ARRIVE, e.getTuileType(5));
		}
		catch(FichierEtapeException e)
		{
			OutilTest.fail("Exception: "+e);
		}
	}
	
	public static void test_b_constructeur()
	{
		try{
			//Carte 5s 17n 4m 4d 16n 7m 3d 17n 5a
			Etape e = new Etape("Firenze - Milano");
			for (int i = 0; i < 78; i++)
			{
				int t = e.getTuileType(i);
				boolean faute = (i < 5 && t != Tuile.DEPART)
							|| (i >=5 && i < 22 && t != Tuile.NORMAL)
							|| (i >= 22 && i < 26 && t != Tuile.MONTE)
							|| (i >= 26 && i < 30 && t != Tuile.DESCENTE)
							|| (i >= 30 && i < 46 && t != Tuile.NORMAL)
							|| (i >= 46 && i < 53 && t != Tuile.MONTE)
							|| (i >= 53 && i < 56 && t != Tuile.DESCENTE)
							|| (i >= 56 && i < 73 && t != Tuile.NORMAL)
							|| (i >= 73 && i < 78 && t != Tuile.ARRIVE);
				OutilTest.assertEquals("La tuile num "+(i+1)+"est incorrecte", false, faute);
			}
			
		} 
		catch(IOException e)
		{
			OutilTest.fail("Exception: "+e);
		} 
		catch(FichierEtapeException e)
		{
			OutilTest.fail("Exception: "+e);
		}
	}
	
	public static void test_c_getLigneDepart()
	{

		try{
			List<Tuile> tuiles = new ArrayList<Tuile>();
			tuiles.add(new Tuile("1s"));
			tuiles.add(new Tuile("1s1m1d"));
			tuiles.add(new Tuile("1n1a"));
			Etape e = new Etape(tuiles);
			Etape e2 = new Etape("Firenze - Milano");
			
			OutilTest.assertEquals("Le depart de l'etape 1", 2, e.getLigneDepart());
			OutilTest.assertEquals("Le depart de l'etape Firenze - Milano", 5, e2.getLigneDepart());
			
		}
		catch(IOException e)
		{
			OutilTest.fail("Exception: "+e);
		}
		catch(FichierEtapeException e)
		{
			OutilTest.fail("Exception: "+e);
		}
	}
	public static void test_d_getLigneArrivee()
	{

		try{
			List<Tuile> tuiles = new ArrayList<Tuile>();
			tuiles.add(new Tuile("1s"));
			tuiles.add(new Tuile("1s1m1d"));
			tuiles.add(new Tuile("1n6a"));
			Etape e = new Etape(tuiles);
			Etape e2 = new Etape("Firenze - Milano");
			
			OutilTest.assertEquals("La fin de l'etape 1", 5, e.getLigneArrivee());
			OutilTest.assertEquals("La fin de l'etape Firenze - Milano", 73, e2.getLigneArrivee());
			
		}
		catch(IOException e)
		{
			OutilTest.fail("Exception: "+e);
		}
		catch(FichierEtapeException e)
		{
			OutilTest.fail("Exception: "+e);
		}
	}
	
	public static void main(String[] args) throws IOException, FichierEtapeException {
		Lanceur.lancer(new TestEtape(), args);
	}

}
