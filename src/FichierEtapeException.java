
@SuppressWarnings("serial") //On n'utilise pas la sérialisation ici
/**
 * Classe d'exception sur les fichiers d'étapes
 * @author Arthur
 */
public class FichierEtapeException extends Exception{
	/**
	 * Constructeur par defaut
	 */
	public FichierEtapeException() {
		super();
	}
	
	/**
	 * Construit l'exception avec un message.
	 * @param m Message de l'erreur.
	 */
	public FichierEtapeException(String m)
	{
		super(m);
	}
}
