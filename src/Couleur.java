import java.util.Arrays;

/**
 * Classe permettant de gérer les couleurs des joueurs et des coureurs.
 * @author Adrien
 *
 */
public class Couleur {
	/**
	 * Identifiant interne de couleur, prenant la valeur d'une des constantes {@link #CODE_ROUGE}, {@link #CODE_VERT}, {@link #CODE_BLEU} ou {@link #CODE_NOIR}.
	 */
	private int couleur;
	/**
	 * Code interne pour la couleur rouge.
	 */
	private static final int CODE_ROUGE = 0;
	/**
	 * Code interne pour la couleur verte.
	 */
	private static final int CODE_VERT = 1;
	/**
	 * Code interne pour la couleur bleue.
	 */
	private static final int CODE_BLEU = 2;
	/**
	 * Code interne pour la couleur noire.
	 */
	private static final int CODE_NOIR = 3;
	/**
	 * Tableau facilitant la conversion de code interne à chaîne de caractère.
	 */
	private static final String[] couleurStr = { "rouge", "vert", "bleu", "noir"};
	/**
	 * Couleur publique correspondant à la couleur rouge.
	 */
	public static final Couleur ROUGE = new Couleur(CODE_ROUGE);
	/**
	 * Couleur publique correspondant à la couleur verte.
	 */
	public static final Couleur VERT = new Couleur(CODE_VERT);
	/**
	 * Couleur publique correspondant à la couleur bleue.
	 */
	public static final Couleur BLEU = new Couleur(CODE_BLEU);
	/**
	 * Couleur publique correspondant à la couleur noire.
	 */
	public static final Couleur NOIR = new Couleur(CODE_NOIR);
	
	/**
	 * Constructeur privé de la classe Couleur.
	 * @param c Code interne de la couleur à créer
	 */
	private Couleur(int c)
	{
		couleur = c;
	}
	
	/**
	 * Méthode permettant de créer une couleur.
	 * @param c Couleur à créer
	 * @return La couleur crée
	 * @throws ValeurIncorrecteException Lève une exception si la couleur spécifiée n'est pas une couleur valide.
	 */
	public static Couleur createCouleur(String c) throws ValeurIncorrecteException
	{
		int couleur = Arrays.asList(couleurStr).indexOf(c.toLowerCase());
		if (couleur == -1)
			throw new ValeurIncorrecteException("La couleur n'est pas valide.");
		return new Couleur(couleur);
	}
	
	/**
	 * Redefinition de la methode equals de la calsse Couleur
	 */
	public boolean equals(Object o)
	{
		return (o != null && (o instanceof Couleur) && couleur == ((Couleur)o).couleur);
	}
	
	/**
	 * Methode toString de la classe Couleur
	 */
	public String toString()
	{
		return couleurStr[couleur];
	}
}
