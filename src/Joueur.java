import java.util.*;

/**
 * Classe modélisant un joueur
 * @author Adrien
 *
 */
public class Joueur {
	/**
	 *  Pioches de cartes du joueur.
	 */
	private PileJoueur cartes;
	/**
	 * Nom ou pseudonyme du joueur.
	 */
	private String nom;
	/**
	 * Couleur du joueur.
	 */
	private Couleur couleur;
	/**
	 * Coureurs du joueur.
	 */
	private HashMap<Integer, Coureur> coureurs;
	/**
	 * Dose de dopage dont le joueur bénéficie.
	 */
	private int doses;
	
	/**
	 * Constructeur d'un joueur, avec une couleur et un nom défini.
	 * @param c Couleur du joueur
	 * @param n Nom ou pseudonyme du joueur
	 * @see Couleur
	 */
	public Joueur(Couleur c, String n)
	{
		nom = n;
		couleur = c;
		cartes = new PileJoueur();
		coureurs = new HashMap<Integer, Coureur>();
		Coureur sprinteur = new Sprinteur(this);
		Coureur rouleur = new Rouleur(this);
		coureurs.put(sprinteur.getType(), sprinteur);
		coureurs.put(rouleur.getType(), rouleur);
		doses = 0;
	}
	
	/**
	 * Permet de piocher 4 cartes pour le coureur spécifié. Si le joueurs ne dispose pas de 4 cartes, retourne les cartes restantes.
	 * @param type Type du coureur pour lequel on pioche. Doit être {@link Coureur#SPRINTEUR} ou {@link Coureur#ROULEUR}.
	 * @throws ValeurIncorrecteException Si type n'est pas un type valide, alors piocher lève une exception.
	 */
	public List<Carte> piocherCartes(int type) throws ValeurIncorrecteException
	{		
		ArrayList<Carte> main = new ArrayList<Carte>();
		for (int i = 0; i < 4; i++)
		{
			Carte c = cartes.piocher(type); //Throws ValeurIncorrecteException
			//Si pas assez de cartes, on arrete de piocher, sinon on ajoute la carte piochee
			if (c == null)
				break;
			else 
				main.add(c); 
		}
		
		return main;
	}
	
	/**
	 * Permet au joueur de choisir une carte parmi une main de carte afin de l'appliquer à son coureur de type donné.
	 * @param main Liste de cartes préalablement piochées grâce à la méthode {@link #piocherCartes(int)} pour ce coureur
	 * @param choix Indice de la carte choisie
	 * @param type Type du coureur concerné
	 * @throws ValeurIncorrecteException Lève une exception si le type de coureur est incorrect.
	 */
	public void choisirCarte(List<Carte> main, int choix, int type) throws ValeurIncorrecteException
	{
		//On garde la carte piochee choisie
		Carte piochee = main.remove(choix);
		getCoureur(type).setCarte(piochee); //Throws ValeurIncorrecteException
		
		//On defausse les autres cartes
		for (Carte c : main)
			cartes.defausser(c, type); //Throws ValeurIncorrecteException
	}
	
	/**
	 * Ajoute une carte fatigue pour le coureur de type donné du joueur
	 * @param type Type du coureur (voir {@link Coureur})
	 * @throws ValeurIncorrecteException Lève une exception si jamais le type est incorrect.
	 */
	public void ajouterFatigue(int type) throws ValeurIncorrecteException
	{
		cartes.defausser(new CarteFatigue(), type);
	}
	
	/**
	 * Retourne le coureur correspondant au type spécifié
	 * @param type Le type de coureur (voir {@link #coureurs.type})
	 * @return Le coureur correspondant au type donné
	 * @throws ValeurIncorrecteException Lève une exception si le type spécifié est invalide.
	 */
	public Coureur getCoureur(int type) throws ValeurIncorrecteException
	{
		Coureur c = coureurs.get(type);
		if (c == null)
			throw new ValeurIncorrecteException("Type de coureur incorrect.");
		return c;
	}
	
	/**
	 * Getter pour l'attribut {@link #couleur}.
	 * @return La couleur du joueur
	 * @see Couleur
	 */
	public Couleur getCouleur()
	{
		return couleur;
	}
	
	/**
	 * Getter pour l'attribut {@link #nom}.
	 * @return Le nom du joueur
	 * @see Couleur
	 */
	public String getNom()
	{
		return nom;
	}
	
	/**
	* Méthode permettant de doper le joueur, avec 1 ou 2 doses.
	* @param dose Nombre de doses (1 à 3)
	* @throws ValeurIncorrecteException Lève une exception si le nombre de doses est incorrect
	*/
	public void doper(int dose) throws ValeurIncorrecteException
	{
		cartes.appliquerDopage(dose);
		doses = dose;
	}
	
	public String toString()
	{
		return getNom()+" (joueur "+couleur.toString()+" - "+(doses == 0 ? "pas de dopage" : doses + " doses de dopage")+")";
	}
}
