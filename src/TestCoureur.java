import libtest.Lanceur;
import libtest.OutilTest;

import java.io.IOException;

/**
 * Une classe de test sur les Coureurs
 * @author Adrien
 * @author Arthur
 */
public class TestCoureur {
	
	public static void test_a_constructeurs()
	{
		Coureur s = new Sprinteur(null);
		Coureur r = new Rouleur(null);
		
		OutilTest.assertEquals("Le type est erroné",  Coureur.SPRINTEUR, s.getType());
		OutilTest.assertEquals("Le type est erroné",  Coureur.ROULEUR, r.getType());
	}
	
	public static void test_b_setInitialPos()
	{
		Coureur c = new Sprinteur(null);
		try{
			c.setInitialPos(2,1);
			OutilTest.assertEquals("La position est erronée",  2, c.getPos());
			OutilTest.assertEquals("La file est erroné",  1, c.getFile());

			try {
				new Sprinteur(null).setInitialPos(1,-1);
				OutilTest.fail("Une valeur incorrecte n'a pas declenché d'exception");
			}catch (ValeurIncorrecteException v){}
			
			c.setInitialPos(4,0);
			OutilTest.assertEquals("La position ne devrait pas changer",  2, c.getPos());
			OutilTest.assertEquals("La file ne devrait pas changer",  1, c.getFile());

		} catch (Exception e)
		{
			OutilTest.fail("Exception: "+e);
		}
	}
	
	public static void test_c_avancer()
	{
		Jeu jeu = new Jeu();
		try{
			//Carte 5s 17n 4m 4d 16n 7m 3d 17n 5a
			jeu.loadEtape("Firenze - Milano");
			
			Coureur c1 = new Sprinteur(null);
			Coureur c2 = new Sprinteur(null);
			Coureur c3 = new Rouleur(null);
			Coureur c4 = new Rouleur(null);
			Coureur c5 = new Sprinteur(null);
			
			//Normal
			c1.setInitialPos(1, 0);
			c1.setCarte(new Carte(8));
			c1.avancer(jeu);
			
			//Monte (avant/apres)
			c2.setInitialPos(15, 0);
			c2.setCarte(new Carte(8));
			c2.avancer(jeu);
			
			c3.setInitialPos(23, 0);
			c3.setCarte(new Carte(8));
			c3.avancer(jeu);
			
			//Descente
			c4.setInitialPos(27, 0);
			c4.setCarte(new Carte(2));
			c4.avancer(jeu);
			
			c5.setInitialPos(70, 1);
			c5.setCarte(new Carte(9));
			c5.avancer(jeu);
			
			
			OutilTest.assertEquals("Coureur 1 : N'a pas bien avance", 9, c1.getPos());
			OutilTest.assertEquals("Coureur 1 : Devrait etre sur la file droite", 0, c1.getFile());
			
			OutilTest.assertEquals("Coureur 2 : N'a pas bien avance", 21, c2.getPos());
			OutilTest.assertEquals("Coureur 2 : Devrait etre sur la file droite", 0, c2.getFile());

			OutilTest.assertEquals("Coureur 3 : N'a pas bien avance", 28, c3.getPos());
			OutilTest.assertEquals("Coureur 3 : Devrait etre sur la file droite", 0, c3.getFile());

			OutilTest.assertEquals("Coureur 4 : N'a pas bien avance", 32, c4.getPos());
			OutilTest.assertEquals("Coureur 4 : Devrait etre sur la file droite", 0, c4.getFile());
			
			OutilTest.assertEquals("Coureur 5 : A trop avance", 77, c5.getPos());
			OutilTest.assertEquals("Coureur 5 : Devrait etre sur la file droite", 0, c5.getFile());
			
		} catch(ValeurIncorrecteException e)
		{
			OutilTest.fail("Exception: "+e);
		} catch(FichierEtapeException e)
		{
			OutilTest.fail("Exception: "+e);
		} catch(IOException e)
		{
			OutilTest.fail("Exception: "+e);
		}catch(PasDeCarteException e)
		{
			OutilTest.fail("Exception: "+e);
		}
	}
	
	public static void test_d_appliquerAspiration()
	{
		Jeu jeu = new Jeu();
		try{
			//Carte 5s 17n 4m 4d 16n 7m 3d 17n 5a
			jeu.loadEtape("Firenze - Milano");
			
			Coureur c1 = new Sprinteur(null);
			Coureur c2 = new Sprinteur(null);
			//Normal
			c1.setInitialPos(1, 1);
			
			//Monte
			c2.setInitialPos(25, 0);
			
			OutilTest.assertEquals("Coureur 1 : Devrait etre aspire", true, c1.appliquerAspiration(jeu));
			OutilTest.assertEquals("Coureur 1 : Mauvaise position", 2, c1.getPos());
			OutilTest.assertEquals("Coureur 1 : Mauvaise file", 1, c1.getFile());
			
			OutilTest.assertEquals("Coureur 2 : Devrait ne pas etre aspire", false, c2.appliquerAspiration(jeu));
			OutilTest.assertEquals("Coureur 2 : Mauvaise position", 25, c2.getPos());
			OutilTest.assertEquals("Coureur 2 : Mauvaise file", 0, c2.getFile());
			
		} catch(ValeurIncorrecteException e)
		{
			OutilTest.fail("Exception: "+e);
		} catch(FichierEtapeException e)
		{
			OutilTest.fail("Exception: "+e);
		} catch(IOException e)
		{
			OutilTest.fail("Exception: "+e);
		}
	}
	
	public static void test_e_compareTo()
	{
		Coureur c1 = new Sprinteur(null);
		Coureur c2 = new Sprinteur(null);
		Coureur c3 = new Rouleur(null);
		Coureur c4 = new Rouleur(null);
		try {
			c1.setInitialPos(1, 0);
			c2.setInitialPos(1, 1);
			c3.setInitialPos(2, 1);
			c4.setInitialPos(4, 0);
		
		}catch(Exception e)
		{
			OutilTest.fail(e.toString());
		}
		OutilTest.assertEquals("Le coureur 1 est devant le coureur 2",  true, c1.compareTo(c2) > 0);
		OutilTest.assertEquals("Le coureur 1 est derriere le coureur 3",  false, c1.compareTo(c3) > 0);
		OutilTest.assertEquals("Le coureur 1 est derriere le coureur 4",  false, c1.compareTo(c4) > 0);
		
		OutilTest.assertEquals("Le coureur 2 est derriere le coureur 3",  false, c2.compareTo(c3) > 0);
		OutilTest.assertEquals("Le coureur 2 est derriere le coureur 4",  false, c2.compareTo(c4) > 0);
		
		OutilTest.assertEquals("Le coureur 3 est derriere le coureur 4",  false, c3.compareTo(c4) > 0);
	}
	
	public static void main(String[] args) {
		Lanceur.lancer(new TestCoureur(), args);
	}

}
