import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe qui crée une étape à partir d'un fichier texte en mettant en place une liste de Tuile
 * @author Arthur
 */
public class Etape {

	/**
	 * Attribut carte d'une étape qui est un tableau de Tuile
	 */
	private ArrayList<Tuile> carte;
	/**
	 * Position de la ligne de départ
	 */
	private int depart;
	/**
	 *  Position de la ligne d'arrivée
	 */
	private int arrivee;
	
	/**
	 * Constructeur d'étape qui prend un paramètre un fichier décrivant une étape avec une Tuile par ligne.
	 * Une tuile est composée d'une suite de groupes de cases.
	 * Un groupe de case est composé du nombre de cases suivi d'un caractère représentant le type de case.
	 * @param destination adresse du fichier représentant une étape
	 * @throws IOException Lève une exception si une erreur de lecture se produit sur le fichier.
	 * @throws FichierEtapeException Lève une exception si il y a un problème avec le format du fichier.
	 */
	public Etape(String destination) throws IOException, FichierEtapeException {
		ArrayList<String> temp = new ArrayList<String>();
		this.carte = new ArrayList<Tuile>();
		destination = ("Map/"+destination);
		BufferedReader lect = new BufferedReader(new FileReader(destination));
		while(lect.ready()) {
			temp.add(lect.readLine());
		}
		lect.close();
		
		if(temp.get(0).charAt(1)!='s' || (Integer.parseInt(""+temp.get(0).charAt(0))!=5 && Integer.parseInt(""+temp.get(0).charAt(0))!=4)) throw new FichierEtapeException("Pas de départ sur cette etape");
		
		String last = temp.get(temp.size()-1);
		if(last.charAt(last.length()-1)!='a') throw new FichierEtapeException("Pas de d'arrivee sur cette etape");

		for(int i =0; i<temp.size();i++)
			carte.add(new Tuile(temp.get(i)));
		
		init();
	}
	
	/**
	 * Constructeur permettant de créer une étape à partir d'une suite de tuile arbitraire.
	 * @param tuiles Les tuiles composant l'étape
	 */
	public Etape(List<Tuile> tuiles) {
		carte = new ArrayList<Tuile>();
		for (Tuile t : tuiles)
			carte.add(t);
		init();
	}
	
	/**
	 * Méthode interne permettant de calculer la position des lignes de départ et d'arrivée
	 */
	private void init()
	{
		int somme = 0;
		for (depart = 0; getTuileType(depart) == Tuile.DEPART; depart++) ;
		for (Tuile t : carte)
			somme += t.getTaille();
		for (arrivee = somme - 1; getTuileType(arrivee - 1) == Tuile.ARRIVE; arrivee--) ;
	}

	/**
	 * Méthode getLigneDepart
	 * @return {@link #depart}
	 */
	public int getLigneDepart()
	{
		return depart;
	}
	/**
	 * Méthode getLigneArrivee
	 * @return {@link #arrivee}
	 */
	public int getLigneArrivee()
	{
		return arrivee;
	}
	
	/**
	 * Méthode getCarte d'une étape
	 * @return {@link #carte}
	 */
	public ArrayList<Tuile> getCarte() {
		return carte;
	}
	
	/**
	 * Fonction qui indique le type de la case sur laquelle se trouve un cycliste
	 * (une montée, une descente, l'arrivée, etc..)
	 * @param pos la position recherchée depuis la premiere case du départ (la case 0)
	 * @return le type de la case 
	 */
	public int getTuileType(int pos) {
		int posTuile = 0;
		int somme = 0;
		while(somme + carte.get(posTuile).getTaille() <= pos) {
			somme += carte.get(posTuile).getTaille();
			posTuile++;
		}
		int posInsideTuile = pos - somme;
		int res = carte.get(posTuile).getComposition().get(posInsideTuile);
		return(res);
	}
	
	/**
	 * Méthode toString d'une étape
	 * @return une affichage tuile par tuile de leur contenu
	 */
	public String toString() {
		StringBuffer str = new StringBuffer("");
		for(int i=0; i<this.carte.size(); i++) {
			str.append(carte.get(i).toString());
			str.append("\n");
		}
		return(str.toString());
	}
	
	/**
	 * Méthode qui vérifie si un nom d'étape est disponible, c'est-à-dire si aucune Etape existante possède ce nom.
	 * @param nomEtape Nom de l'étape à tester
	 * @return Retourne vrai si le nom est disponible et faux si le nom est déjà utilisé
	 */
	public static boolean nomEtapeDisponible(String nomEtape) {
		return !new File("Map/"+nomEtape).exists();
	}
	
}
