import libtest.Lanceur;

/**
 * Une classe de test lançant tous les autres classes tests.
 * @author Adrien
 * @author Arthur
 *
 */
public class TousLesTests {

	public static void main(String[] args) {
		Lanceur.lancer(new TestEtape(), args);
		Lanceur.lancer(new TestCoureur(), args);
		Lanceur.lancer(new TestGroupe(), args);
		Lanceur.lancer(new TestJeu(), args);
	}

}
