import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Classe permettant de gérer les groupes de coureurs, notamment dans la phase d'aspiration et de fatigue.
 * @author Adrien
 * @author Arthur
 */
public class Groupe {
	
		/**
		 * Liste de coureurs composant un groupe.
		 */
		private List<Coureur> coureurs;
		
		/**
		 * Constructeur de Groupe qui initialise {@link #coureurs} à vide.
		 */
		public Groupe()
		{
			coureurs = new ArrayList<Coureur>();
		}
		
		/**
		 * Getter de la taille de la liste {@link #coureurs}
		 * @return un entier répresentant la taille de la liste coureurs.
		 */
		public int getTaille()
		{
			return coureurs.size();
		}
		
		/**
		 * Méthode interne qui ordonne la liste {@link #coureurs}, par ordre décroissant de position.
		 */
		private void ordonner()
		{
			coureurs.sort(null);
			Collections.reverse(coureurs);
		}
		
		/**
		 * Méthode qui permet d'ajouter un Coureur c à la liste {@link #coureurs}.
		 * @param c le Coureur à ajouter.
		 */
		public void ajouter(Coureur c)
		{
			if (c == null)
				throw new NullPointerException("Le coureur est null.");
			coureurs.add(c);
			ordonner();
		}
		
		/**
		 * Méthode qui permet d'ajouter une List<Coureur> à la liste {@link #coureurs}.
		 * @param c la List<Coureur> à ajouter.
		 */
		public void ajouter(List<Coureur> c)
		{
			coureurs.addAll(c);
			ordonner();
		}
		
		/**
		 * Méthode qui permet de faire avancer tout le groupe de coureurs, au travers de la méthode {@link Coureur#appliquerAspiration(Jeu)}.
		 * Retourne l'indice dans la liste du dernier coureur à pouvoir bénéficier de l'aspiration (dernier coureur du groupe avant une montée).
		 * @param j Jeu actuel
		 * @return Retourne l'indice dans la liste du dernier coureur à être aspiré.
		 */
		public int avancer(Jeu j)
		{
			int i = 0;
			for (Coureur c : coureurs)
			{
				if (!c.appliquerAspiration(j))
					return i;
				i++;
			}
			return i;
		}
		
		/**
		 * Methode qui retourne le Coureur à l'index passé en parametre dans {@link #coureurs}.
		 * @param i l'index de la position dont on veut le Coureur dans la liste.
		 * @return un Coureur ou null si on sort du tableau.
		 */
		public Coureur get(int i)
		{
			try {
				return coureurs.get(i);
			} catch (IndexOutOfBoundsException e)
			{
				return null;
			}
		}
		
		/**
		 * Supprime de la liste et retourne le coureur à un indice donné.
		 * @param i Indice du coureur à supprimer
		 * @return Retourne le coureur supprimé ou null si l'indice est invalide.
		 */
		public Coureur remove(int i)
		{
			try {
				return coureurs.remove(i);
			} catch (IndexOutOfBoundsException e)
			{
				return null;
			}
		}
		
		/**
		 * Méthode qui renvoit le premier coureur du groupe.
		 * @return Retourne le coureur en tête de groupe.
		 */
		public Coureur getFirst()
		{
			try {
				return coureurs.get(0);
			} catch (IndexOutOfBoundsException e)
			{
				return null;
			}
		}
		
		/**
		 * Méthode qui renvoit le dernier coureur du groupe.
		 * @return Retourne le coureur en queue de groupe.
		 */
		public Coureur getLast()
		{
			try {
				return coureurs.get(coureurs.size() - 1);
			} catch (IndexOutOfBoundsException e)
			{
				return null;
			}
		}

		/**
		 * Permet de récupérer le ou les coureur(s) qui sont sur la première case du groupe, et qui seront soumis à la fatigue.
		 * @return Retourne une liste d'un ou de deux coureurs de tête de groupe.
		 */
		public List<Coureur> getCoureursTete() {
			
			if (getTaille() == 0)
				return null;
			
			List<Coureur> tete = new ArrayList<Coureur>();
			tete.add(coureurs.get(0));
			
			if (getTaille() >= 2 && coureurs.get(0).getPos() == coureurs.get(1).getPos())
				tete.add(coureurs.get(1));
			
			return tete;
		}
	}