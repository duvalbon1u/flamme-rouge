import java.util.ArrayList;
/**
 * Classe représentant une Tuile de l'étape.
 * @author Arthur
 */
public class Tuile {
	
	/**
	 * Constante indiquant la file de droite d'une Tuile
	 */
	public static final int FILE_DROITE = 0;
	
	/**
	 * Constante indiquant la file de gauche d'une Tuile
	 */
	public static final int FILE_GAUCHE = 1;
	
	/**
	 * Constante indiquant que le type de la case de cette Tuile est une case départ
	 */
	public static final int DEPART = 2;
	
	/**
	 * Constante indiquant que le type de la case de cette Tuile est une case descente
	 */
	public static final int DESCENTE = 3;
	
	/**
	 * Constante indiquant que le type de la case de cette Tuile est une case monté
	 */
	public static final int MONTE = 4;
	
	/**
	 * Constante indiquant que le type de la case de cette Tuile est une case normal
	 */
	public static final int NORMAL = 5;
	
	/**
	 * Constante indiquant que le type de la case de cette Tuile est une case arrivée
	 */
	public static final int ARRIVE = 6;
	
	/**
	 * Attribut tableau détaillant la composition d'une Tuile case par case
	 */
	private ArrayList<Integer> composition;
	
	
	/**
	 * Constructeur qui à partir d'un String codifié crée une Tuile
	 * @param contient String contenant les parametres internes de la futur Tuile
	 * @throws FichierEtapeException exception traitant les erreurs de syntaxes dans les paramètres de génération
	 */
	public Tuile(String contient) throws FichierEtapeException {
		this.composition = new ArrayList<Integer>();
		for(int i=0; i<contient.length(); i+=2) {
			char c;
			int type;
			//On vérifie que le le premier caractère d'un couple est bien une chiffre valide
			if(!(Character.isDigit(contient.charAt(i)))){
				throw new FichierEtapeException("Un emplacement reserve a un chiffre contient une autre valeur");
			}
			else
				c = contient.charAt(i+1);
			//On vérifie que le le second caractère d'un couple est bien une lettre valide
			switch(c) {
				case (char)'m':
					type = Tuile.MONTE;
					break;
				case (char)'d':
					type = Tuile.DESCENTE;
					break;
				case (char)'a':
					type = Tuile.ARRIVE;
					break;
				case (char)'s':
					type = Tuile.DEPART;
					break;
				case (char)'n':
					type = Tuile.NORMAL;
					break;
				default :
					throw new FichierEtapeException("Un emplacement reserve a un caractere contient une autre valeur");
			}
			for(int j=0; j<(Integer.parseInt(""+contient.charAt(i))); j++) {
				composition.add(type);
			}
		}
	}
	
	/**
	 * Méthode getComposition d'une Tuile
	 * @return l'attribut composition d'une Tuile
	 */
	public ArrayList<Integer> getComposition() {
		return composition;
	}
	
	/**
	 * Donne le nombre de case de la tuile, un raccourci pour {@link #getComposition()}.{@link java.util.List#size() size()}.
	 * @return La taille en cases de la tuile
	 */
	public int getTaille()
	{
		return composition.size();
	}
	
	/**
	 * Méthode toString d'une Tuile
	 * @return sur une ligne la composition d'une tuile case par case
	 */
	public String toString() {
		StringBuffer str = new StringBuffer("");
		for(int i=0; i<this.composition.size(); i++) {
			str.append(this.composition.get(i));
		}
		return(str.toString());
	}
}
