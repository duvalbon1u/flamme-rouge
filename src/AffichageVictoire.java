import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

/**
 * Classe permettant de glorifier le vainqueur
 * @author Arthur
 *
 */
@SuppressWarnings("serial")
public class AffichageVictoire extends JPanel{
	
	/**
	 * Attribut pseudoGagnant qui est le nom du joueur gagnant
	 */
	private String pseudoGagnant;
	/**
	 * Attribut couleurGagnant qui est la {@link Couleur} du joueur gagnant
	 */
	private Color couleurGagnant;
	/**
	 * Tableau de deux entier qui gère la taille du cercle de victoire
	 */
	private int[] coords = new int[2];
	/**
	 * Boolean qui gère le grossissement du cercle de victoire
	 */
	private boolean monter = true;
	/**
	 * Compteur qui gère le grossissement du cercle
	 */
	private int compteur = 0;
	/**
	 * JLabel qui gère l'affichage du joueur victorieux
	 */
	private JLabel label;
	
	/**
	 * Constructeur de l'affichage de victoire
	 * @param g le gagnant
	 */
	public AffichageVictoire(Coureur g) {
		this.setLayout(new BorderLayout());
		this.pseudoGagnant = g.getJoueur().getNom();
		switch(g.getJoueur().getCouleur().toString()) {
			case ("bleu"):
				this.couleurGagnant = Color.BLUE;
				break;
			case ("vert"):
				this.couleurGagnant = Color.GREEN;
				break;
			case ("noir"):
				this.couleurGagnant = Color.BLACK;
				break;
			case ("rouge"):
				this.couleurGagnant = Color.RED;
				break;		
		}
		//tableau
		coords[0] = -150;
		coords[1] = 300;
		Font police = new Font("Verdana", Font.PLAIN, 25);
		this.label = new JLabel("Felicitation "+pseudoGagnant);
		this.label.setFont(police);
		this.label.setForeground(Color.WHITE);
		this.label.setVerticalAlignment(SwingConstants.CENTER);
		this.label.setHorizontalAlignment(SwingConstants.CENTER);
		this.add(label, BorderLayout.CENTER);
		this.setBackground(new Color(230,230,230));
	}
	
	/**
	 * Méthode qui met à jour l'affichage
	 */
	public void affichage() {
		if(monter) {
			compteur++;
			coords[0]--;
			coords[1]+=2;
			if(compteur>49) { monter = false;}
		} else {
		compteur--;
		coords[0]++;
		coords[1]-=2;
		if(compteur<1) {monter = true;}
		}
		this.repaint();
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		int h = this.getHeight();
		int w = this.getWidth();
		g.setColor(this.couleurGagnant);
		g.fillOval((w/2)+coords[0], (h/2)+coords[0], coords[1], coords[1]);
	}
}
