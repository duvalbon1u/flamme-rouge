import java.awt.Dimension;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

import javax.swing.JFrame;
/**
 * Classe permettant d'externaliser les interactions avec les utilisateurs, et ainsi de les modifier plus facilement.
 * @author Adrien
 * @author Arthur
 */
public class Interaction{
	
	/**
	 * Scanner utilisé pour le dialogue avec l'utilisateur.
	 * Ne doit pas être fermé car le flux System.in est géré par la JVM et non par le programme.
	 */
	private static final Scanner sc = new Scanner(System.in);
	
	/**
	 * Fenêtre d'affichage de la partie
	 */
	private static JFrame fenetre = new JFrame("Circuit");
	
	/**
	 * Méthode interne permettant de saisir un entier valide.
	 * @return L'entier saisi ou -1 en cas d'erreur
	 */
	private static int saisirEntier()
	{
		int ret = -1;
		try {
			ret = sc.nextInt();
			sc.nextLine();
		} catch(InputMismatchException ex)
		{
			System.out.println("Veuillez saisir un entier valide.");
			sc.nextLine();
		}
		return ret;
	}
	
	/**
	 * Méthode de présentation du jeu à l'utilisateur.
	 */
	public static void presentation()
	{
		System.out.println("Bienvenue sur le jeu Flamme Rouge!\n");
		System.out.println("Dans le monde du cyclisme, la flamme rouge est un drapeau qui indique le dernier kilomètre : la dernière ligne droite, le moment où il faut tout donner !\n" + 
				"Ce matin, la banlieue de Paris fourmille de cyclistes venus du monde entier pour disputer une course mémorable, sous les yeux de nombreux spectateurs.\n" + 
				"Qui passera la ligne d’arrivée le premier et inscrira son nom dans l’Histoire ?\n" + 
				"Stratégie et endurance seront les clés de la victoire. Que la meilleure équipe gagne !");
	}
	
	/**
	 * Méthode de sélection du nombre de joueur, ainsi que du pseudonyme et de la couleur du joueur
	 * @param j Jeu auquel la méthide ajoute les joueurs
	 */
	public static void initJoueurs(Jeu j)
	{
		int nb = -1;
		do {
			System.out.print("Choisissez le nombre de joueurs (2-4): ");
			nb = saisirEntier();
		} while (nb < 2 || nb > 4);

		System.out.println("Le premier joueur est le dernier à être monté en vélo (ou le plus jeune).");
		System.out.println("Si un des joueurs est débutant, vous pouvez le doper pour l'aider, avec une dose allant de 1 à 3.");
		for (int i = 0; i < nb; i++)
		{
			String s = "";
			do {
				System.out.print("Pseudonyme du joueur "+(i+1)+" : ");
				s = sc.nextLine();
			} while (s.equals(""));
			
			Joueur joueur = null;
			boolean ajoute = false;
			do {
				System.out.print("Couleur du joueur (noir, vert, bleu, rouge) : ");
				try {
					String c = sc.nextLine();
					joueur = j.ajouterJoueur(s, c);
					ajoute = true;
				}catch(ValeurIncorrecteException e)
				{
					System.out.println("Veuillez saisir une couleur valide.");
				}catch(CouleurUtiliseeException e)
				{
					System.out.println("Cette couleur est déjà utilisée.");
				}
			}while (!ajoute);
			
			int dopage;
			do{
				System.out.print("Dose de dopage à administrer (0 - 3) : ");
				dopage = saisirEntier();
			} while(dopage < 0 || dopage > 3);
			
			try{
				joueur.doper(dopage);
			}catch(ValeurIncorrecteException e)
			{
				System.err.println("Erreur au dopage : "+e);
			}
		}
	}
	
	/**
	 * Méthode qui affiche toutes les étapes disponibles dans le répertoire "Map"
	 * @param folder le nom du fichier contenant les étapes
	 */
	public static void listeDesMaps(File folder) {
	    for (File fileEntry : folder.listFiles()) {
	            System.out.println("- "+fileEntry.getName());
	    }
	}
	
	/**
	 * Méthode d'interaction avec l'utilisateur pour choisir létapes à utiliser
	 * @return Le nom de l'étape sélectionnée
	 */
	public static String choisirEtape()
	{
		System.out.println("Choisissez votre étape parmis les suivantes : ");
		File map = new File("Map");
		listeDesMaps(map);
		return sc.nextLine();
	}
	
	/**
	 * Méthode d'interaction qui demande à l'utilisateur si il veut créer sa propre Etape
	 */
	public static void demandeCreerEtape()
	{
		String rep = "";
		do {
			System.out.println("Voulez-vous créer votre propre étape ? (oui ou non)");
			rep = sc.nextLine();
		} while(!rep.equals("oui") && !rep.equals("non"));
		//Boucle sur la réponse
		while(rep.equals("oui")) {
			try
			{
				creerEtape();
			}catch(IOException e)
			{
				System.out.println("Erreur lors de la création de l'étape :"+ e);
			}
			System.out.println("Voulez-vous créer une autre étape ? (oui ou non)");
			rep = sc.nextLine();
		}
	}
	
	
	
	/**
	 * Méthode qui crée une nouvelle étape en interaction avec l'utilisateur
	 * @throws IOException Si une erreur d'écriture survient.
	 */
	public static void creerEtape() throws IOException {
		final int LIGNE_DROITE = 0;
		final int VIRAGE_LEGER = 1;
		final int VIRAGE_SERRE = 2;
		int[] tabTuile = new int[3];
		//initialisation du tableau des Tuiles restantes
		tabTuile[LIGNE_DROITE] = 7; tabTuile[VIRAGE_LEGER] = 6; tabTuile[VIRAGE_SERRE] = 6;
		
		//Nom de l'étape
		System.out.println("Comment souhaitez vous nommer votre étape ?");
		String nomEtape = sc.nextLine();
		while(!Etape.nomEtapeDisponible(nomEtape)) {
			System.out.println("Ce nom est déja utilisé");
			System.out.println("Veuillez choisir un autre nom pour votre étape : ");
			nomEtape = sc.nextLine();
		}
		BufferedWriter ecrivain = new BufferedWriter(new FileWriter("Map/"+nomEtape));  
		
		//Nombre de tuile
		int nbrTuile = 0;
		do {
			System.out.println("Combien de tuiles voulez vous utiliser ? (1 - 19)");
			System.out.println("(Tuile de départ et d'arrivée non comprises)");
			nbrTuile = saisirEntier();
		} while(nbrTuile <= 0 || nbrTuile>19);
		
		//Génération des tuiles
		int nbrCase;
		String typeCase = "";
		
			//Tuile départ
			do {
				System.out.println("Voulez-vous une ligne de départ de 4 ou de 5 cases ?");
				nbrCase = saisirEntier();
			} while(nbrCase!=5 && nbrCase!=4);
			ecrivain.write(nbrCase+"s");
			nbrCase = 6-nbrCase;
			do {
				System.out.println("Les "+nbrCase+" cases restantes seront de quel type ?");
				System.out.println("'normal', 'montée' ou 'descente'");
				typeCase = sc.nextLine();
			} while(!typeCase.equals("normal") && !typeCase.equals("montée") && !typeCase.equals("montee") && !typeCase.equals("descente"));
			ecrivain.write(Integer.toString(nbrCase));
			ecrivain.write(typeCase.charAt(0));
			ecrivain.newLine();
			
			//Corps de l'Etape
			int typeTuile = 0;
			boolean tuileDisponible;
			for(int i=0; i<nbrTuile; i++) {
				tuileDisponible = false;
				String sLigneDroite = tabTuile[LIGNE_DROITE] < 2 ? "" : "s";
				String sVirageLeger = tabTuile[VIRAGE_LEGER] < 2 ? "" : "s";
				String sVirageSerre = tabTuile[VIRAGE_SERRE] < 2 ? "" : "s";
				String sTuile = tabTuile[LIGNE_DROITE] < 2 ? "" : "s";
				System.out.println("Il reste "+tabTuile[LIGNE_DROITE]+" ligne"+sLigneDroite+" droite"+sLigneDroite);
				System.out.println("Il reste "+tabTuile[VIRAGE_LEGER]+" virage"+sVirageLeger+" léger"+sVirageLeger);
				System.out.println("Il reste "+tabTuile[VIRAGE_SERRE]+" virage"+sVirageSerre+" serré"+sVirageSerre);
				System.out.println("Il vous reste "+(nbrTuile-i)+" tuile"+sTuile+" à placer");
				
				//choix de la tuile
				while(!tuileDisponible) {
					do {
						System.out.println("Quel sera le type de votre prochaine tuile ?");
						System.out.println("0. Une ligne\n1. Un virage léger\n2. Un virage serré");
						System.out.print("Choix : ");
						typeTuile = saisirEntier();
					} while(typeTuile!=0 && typeTuile!=1 && typeTuile!=2);
					
					if(typeTuile==0 && tabTuile[LIGNE_DROITE]<1) {
						System.out.println("Il n'y a plus de ligne droite disponible.");
						tuileDisponible = false;
					}
					else if(typeTuile==1 && tabTuile[VIRAGE_LEGER]<1) {
						System.out.println("Il n'y a plus de virage léger disponible.");
						tuileDisponible = false;
					} else if(typeTuile==2 && tabTuile[VIRAGE_SERRE]<1) {
						System.out.println("Il n'y a plus de virage serré disponible.");
						tuileDisponible = false;
					}
					else {
						tuileDisponible = true;
					}
				}
				//Compositio de la Tuile
				int choixCompo;
				
				//Ligne Droite
				if(typeTuile == 0) {
					do {
						System.out.println("Voici les compositions de ligne droite disponibles :");
						System.out.println("1. {normal, normal, normal, normal, normal, normal}");
						System.out.println("2. {montée, montée, montée, descente, descente, descente}");
						System.out.println("3. {descente, descente, descente, montée, montée, montée}");
						System.out.println("4. {montée, montée, montée, montée, normal, normal}");
						System.out.println("5. {normal, normal, montée, montée, montée, montée}");
						System.out.println("6. {descente, descente, descente, descente, normal, normal}");
						System.out.println("7. {normal, normal, descente, descente, descente, descente}");
						System.out.println("Choisissez la composition de votre Tuile (1-7)");
						choixCompo = saisirEntier();
					} while(choixCompo<1 || choixCompo>7);
					
					String compo = new String();
					switch (choixCompo) {
						case 1 :
							compo = "6n";
							break;
						case 2 :
							compo = "3m3d";
							break;
						case 3 :
							compo = "3d3m";
							break;
						case 4 :
							compo = "4n2m";
							break;
						case 5 :
							compo = "2n4m";
							break;
						case 6 :
							compo = "4d2n";
							break;
						case 7 :
							compo = "2n4d";
							break;
					}
					ecrivain.write(compo);
					ecrivain.newLine();
				}
				//Virage leger/serre
				else {
					do {
						if(typeTuile==1) System.out.println("Voici les compositions de virage léger disponibles :");
						else System.out.println("Voici les compositions de virage serré disponibles :");
						System.out.println("1. {normal, normal}");
						System.out.println("2. {montée, montée}");
						System.out.println("3. {descente, descente}");
						choixCompo = saisirEntier();
					} while(choixCompo<1 || choixCompo>3);
					String compo = new String();
					switch (choixCompo) {
						case 1 :
							compo = "2n";
							break;
						case 2 :
							compo = "2m";
							break;
						case 3 :
							compo = "2d";
							break;
					}
					ecrivain.write(compo);
					ecrivain.newLine();
				}
				tabTuile[typeTuile]-=1;
			}
			//Tuile arrivée
			do {
				System.out.println("Quelle sera type de la dernière case avant l'arrivée ?");
				System.out.println("'normal', 'montée' ou 'descente' ?");
				typeCase = sc.nextLine();
			} while(!typeCase.equals("normal") && !typeCase.equals("montée") && !typeCase.equals("montee") && !typeCase.equals("descente") );
			ecrivain.write("1"+typeCase.charAt(0));
			ecrivain.write("5a");
			ecrivain.close();
			System.out.println("Votre étape est prête à être utilisée !");
	}
	
	/**
	 * Permet de placer les coureurs pour un joueur
	 * @param j Joueur dont on veut placer les joueurs
	 * @param jeu Jeu actuel
	 */
	public static void placerCoureurs(Joueur j, Jeu jeu)
	{
		System.out.println("Placement des coureurs du joueur "+j);
		int[] types = {Coureur.ROULEUR, Coureur.SPRINTEUR};
		String[] typesStr = {"rouleur", "sprinteur"};
		
		int nbMax = jeu.getEtape().getLigneDepart() - 1;
		for (int i = 0; i < 2; i++)
		{
			int c = 0, file = 0;
			boolean erreur;
			do{
				erreur = false;
				do{
					System.out.println("Choisissez la case de votre "+typesStr[i]+" (0 - "+nbMax+") :");
					c = saisirEntier();
				}while(c < 0 || c > nbMax);
				
				String fileStr = new String();
				do{
					System.out.println("Choisissez la file de votre "+typesStr[i]+" (Droite ou Gauche) :");
					fileStr = sc.nextLine().toLowerCase();
					
					if (fileStr.equals("droite"))
						file = Tuile.FILE_DROITE;
					else if (fileStr.equals("gauche"))
						file = Tuile.FILE_GAUCHE;
					else
					{
						file = -1;
						System.out.println("Veuillez choisir une file valide");
					}
				} while (file == -1);
				
				if (jeu.estPlacePleine(c,file, null))
				{
					erreur = true;
					System.out.println("La place est occupée!");
				}
				else {
					try{
						j.getCoureur(types[i]).setInitialPos(c, file);
					} catch (ValeurIncorrecteException e)
					{
						System.err.println("Case ou file incorrecte: "+e.getMessage());
						erreur = true;
					}
					
				}
			} while(erreur);
			afficherJeu(jeu);
		}
	}
	
	/**
	 * Permet de choisir pour un joueur quel type de carte il veut piocher en premier.
	 * @param j joueur concerné
	 * @return type choisi: {@link Coureur#ROULEUR} ou {@link Coureur#SPRINTEUR}
	 */
	public static int typePremierPiocheChoix(Joueur j)
	{
		System.out.println("Joueur "+j+" :");
		System.out.println("Choisissez le type du coureur pour lequel vous allez piocher en premier : ");
		System.out.println(Coureur.SPRINTEUR+" : Sprinteur");
		System.out.println(Coureur.ROULEUR+" : Rouleur");
		
		return saisirEntier();
	}
	
	/**
	 * Interaction de pioche de carte pour un joueur lors de la phase 1.
	 * Demande à l'utilisateur de choisir une carte parmi les cartes présentées.
	 * @param cartes Liste de carte parmi lesquelles l'utilisateur devrait choisir. Sa longueur ne devrait pas excéder 4.
	 * @param type Type du coureur pour lequel on pioche
	 * @return L'index de la carte choisie par l'utilisateur
	 */
	public static int piocherChoix(List<Carte> cartes, int type)
	{
		System.out.println("Pour le "+(type == Coureur.ROULEUR ? "rouleur" : "sprinteur"));
		System.out.println("Choisissez une carte :");
		for (int i = 0; i < cartes.size(); i++)
			System.out.println((i+1)+". "+cartes.get(i));
		
		int val = -1;
		while (val < 1 || val > 4) {
			System.out.print("Votre choix (1-4): ");
			val = saisirEntier();
		}
		return val - 1;
	}
	
	/**
	 * Méthode qui fait l'initialisation de l'affichage de la partie à partir d'un Jeu j.
	 * @param j le Jeu courant.
	 */
	public static void initAffichage(Jeu j)
	{
		AffichageJeu a = new AffichageJeu(j);
		a.setPreferredSize(new Dimension(651, 400));
		fenetre.setContentPane(a);
		fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		fenetre.pack();
	}
	
	/**
	 * Méthode qui affiche la fenêtre dans laquelle se passe la partie.
	 * @param j le Jeu courant.
	 */
	public static void afficherJeu(Jeu j)
	{
		fenetre.setVisible(true);
		fenetre.repaint();
	}
	
	/**
	 * Méthode qui affiche les résultats de la partie à partir du joueur gagnant.
	 * @param g le Joueur gagnant.
	 */
	public static void affichageVictoire(Coureur g){
		System.out.println("Le "+g.typeCoureur()+" de l'équipe "+g.getJoueur().getCouleur()+" est le premier a avoir dépassé la ligne d'arrivée,");
		System.out.println("Felicitation "+g.getJoueur().getNom()+" votre équipe a gagné !!!");
		fenetre.setContentPane(new AffichageVictoire(g));
		AffichageVictoire victory = (AffichageVictoire) fenetre.getContentPane();
		fenetre.validate();
		while(true) {
			victory.affichage();
			try{Thread.sleep(50);} catch(InterruptedException e) {}
		}
	}
	
	
}
