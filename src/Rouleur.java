/**
 * Classe créant un rouleur.
 * @author Adrien
 *
 */
public class Rouleur extends Coureur {
	/**
	 * Construit un coureur de type Rouleur.
	 */
	public Rouleur(Joueur j)
	{
		super(ROULEUR, j);
	}
}
