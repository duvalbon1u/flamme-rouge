/**
 * Classe représentant une carte énergie.
 * @author Adrien
 *
 */
public class Carte{
	/**
	 * Valeur de la carte.
	 */
	private int valeur;
	/**
	 * Type de la carte, en minuscule: énergie ou fatigue.
	 */
	protected String type;
	
	/**
	 * Construit une carte énergie avec une valeur donnée.
	 * @param val Valeur de la carte à créer
	 */
	public Carte(int val)
	{
		valeur = val;
		type = "energie";
	}
	
	/**
	 * Getter pour l'attribut {@link #valeur}.
	 * @return La valeur de la carte
	 */
	public int getValeur()
	{
		return valeur;
	}
	
	/**
	 * Getter pour l'attribut {@link #type}.
	 * @return Le type de la carte
	 */
	public String getType()
	{
		return type;
	}
	
	/**
	 * Methode toString de la class Carte
	 */
	public String toString() {
		return "Carte "+type+" de valeur "+getValeur();
	}
}
