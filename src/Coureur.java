/**
 * Classe représentant un coureur
 * @author Adrien
 * @author Arthur
 */
public class Coureur implements Comparable<Coureur>{
	/**
	 * Type du coureur, soit {@link #SPRINTEUR} ou {@link #ROULEUR}.
	 */
	private int type;
	/**
	 * Position sur l'étape du joueur, index du tableau de cases.
	 */
	private int pos;
	/**
	 * File du joueur, soit {@link Tuile#FILE_DROITE} ou {@link Tuile#FILE_GAUCHE}.
	 */
	private int file;
	/**
	 * Joueur auquel appartient ce Coureur.
	 */
	private Joueur joueur;
	/**
	 * Carte du joueur pour ce tour ci
	 */
	private Carte carte;
	/**
	 * Type publique de coureur correspondant au sprinteur.
	 */
	public final static int SPRINTEUR = 0;
	/**
	 * Type publique de coureur correspondant au rouleur.
	 */
	public final static int ROULEUR = 1;
	
	/**
	 * Construit un coureur de type spécifié, utilisée par les constructeur des classes dérivées.
	 * @param t Type du coureur à créer, soit {@link #SPRINTEUR} ou {@link #ROULEUR}.
	 * @param j Joueur auquel appartient le coureur
	 */
	protected Coureur(int t, Joueur j)
	{
		type = t;
		pos = -1;
		file = -1;
		joueur = j;
	}
	
	/**	
	 * Définit la position initiale du coureur, ne fonctionne que si cette méthode n'a jamais été appelée sur ce coureur.
	 * @param p Position du coureur (voir {@link #pos})
	 * @param f File du joueur (voir {@link #file})
	 * @return Vrai si la position a été changée, faux si le coureur a déjà été initialisé
	 * @throws ValeurIncorrecteException Lève une exception si la position ou la file est invalide.
	 */
	public boolean setInitialPos(int p, int f) throws ValeurIncorrecteException
	{
		if (pos != -1 && file != -1)
			return false;
		pos = p;
		if (f != Tuile.FILE_DROITE && f != Tuile.FILE_GAUCHE)
			throw new ValeurIncorrecteException("File incorrecte.");
		file = f;
		return true;
	}
	
	/**
	 * Setter de l'attribut {@link #carte}
	 * @param c la nouvelle valeur Carte de l'attribut.
	 */
	public void setCarte(Carte c)
	{
		carte = c;
	}
	
	/**
	 * Methode qui applique l'aspiration pour ce coureur : si le joueur n'est pas en montée, il avance d'une case.
	 * @param j Le Jeu actuellement en cours.
	 * @return Retourne vrai si une aspiration a été faite et false si ce n'est pas le cas.
	 */
	public boolean appliquerAspiration(Jeu j)
	{
		if (j.getEtape().getTuileType(pos) == Tuile.MONTE)
			return false;
		pos++;
		return true;
	}
	
	/**
	 * Permet de faire avancer un coureur en utilisant la carte lui étant attribué. 
	 * Une carte doit lui avoir été associée grâce à la méthode {@link #setCarte(Carte)}.
	 * Un coureur est limité à un mouvement de 5 cases s'il passe par une montée et fait un mouvement d'au moins 5 cases s'il part d'une descente.
	 * @param j Le Jeu en cours
	 * @throws PasDeCarteException Lève une exception si aucune carte n'a été définie pour ce coureur.
	 */
	public void avancer(Jeu j) throws PasDeCarteException
	{
		if (carte == null)
			throw new PasDeCarteException();
		int val = carte.getValeur();
		
		int tuileT = j.getEtape().getTuileType(pos);
		if (tuileT == Tuile.DESCENTE)
			val = Math.max(val, 5);
		else if (tuileT == Tuile.MONTE)
			val = Math.min(val, 5);
		
		for (int i = 0; i < val; i++)
		{
			pos++;
			try {
				if (j.getEtape().getTuileType(pos + 1) == Tuile.MONTE)
					val = Math.min(val, 5);
			} catch(IndexOutOfBoundsException e)
			{
				break;
			}
		}
		
		carte = null;
		while (j.estCasePleine(pos, this))
			pos--;
		
		file = j.estCaseVide(pos, this) ? Tuile.FILE_DROITE : Tuile.FILE_GAUCHE;
	}

	/**
	 * Getter pour l'attribut {@link #type}
	 * @return Le type du coureur.
	 */
	public int getType() {
		return type;
	}

	/**
	 * Getter pour l'attribut {@link #pos}
	 * @return La position du coureur.
	 */
	public int getPos() {
		return pos;
	}

	/**
	 * Getter pour l'attribut {@link #file}
	 * @return La file du coureur.
	 */
	public int getFile() {
		return file;
	}
	
	/**
	 * Getter pour l'attribut  {@link #joueur}
	 * @return le Joueur qui possede ce coureur.
	 */
	public Joueur getJoueur() {
		return joueur;
	}

	@Override
	public int compareTo(Coureur c) {
		return (pos > c.pos ||(pos == c.pos && file == Tuile.FILE_DROITE)) ? 1 : -1;
	}
	
	public String toString()
	{
		return (type == Coureur.ROULEUR ? "Rouleur" : "Sprinteur") + " ("+pos+";"+file+") de "+joueur;
	}
	
	/**
	 * Methode qui indique le type de Coureur de ce coureur
	 * @return "Rouleur" ou "Sprinter" en fonction du type du coureur
	 */
	public String typeCoureur() {
		return (type == Coureur.ROULEUR ? "Rouleur" : "Sprinteur");
	}
	
}
