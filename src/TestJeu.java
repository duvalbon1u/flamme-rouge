import java.io.IOException;
import java.util.List;

import libtest.Lanceur;
import libtest.OutilTest;

/**
 * Une Classe de test sur le Jeu.
 * @author Adrien 
 * @author Arthur
 *
 */
public class TestJeu {
	
	/**
	 * Création d'un Jeu pour réaliser les tests
	 */
	public static void test_a_ajouterJoueur()
	{
		Jeu jeu = new Jeu();
		try{
			jeu.ajouterJoueur("j1", "rouge");
			jeu.ajouterJoueur("j2", "vert");
			try{
				jeu.ajouterJoueur("j1", "rouge");
				OutilTest.fail("Devrait lancer une exception CouleurUtilisee");
			}catch(CouleurUtiliseeException e) {}
		} catch(Exception e)
		{
			OutilTest.fail("Exception: "+e);
		}
		OutilTest.assertEquals("Il devrait y avoir 4 coureurs",4, jeu.getCoureurs().size());
	}
	
	public static void test_b_getCoureursOrdonnes()
	{
		Jeu jeu = new Jeu();
		Joueur j1 = null, j2 = null;
		try{
			j1 = jeu.ajouterJoueur("j1", "rouge");
			j2 = jeu.ajouterJoueur("j2", "vert");
			j1.getCoureur(Coureur.ROULEUR).setInitialPos(0, 0);
			j1.getCoureur(Coureur.SPRINTEUR).setInitialPos(0, 1);
			j2.getCoureur(Coureur.SPRINTEUR).setInitialPos(2, 0);
			j2.getCoureur(Coureur.ROULEUR).setInitialPos(3, 1);
			
			List<Coureur> c = jeu.getCoureursOrdonnes(false);
			OutilTest.assertEquals("Coureur invalide a la place 1 en ordre croissant",j2.getCoureur(Coureur.ROULEUR), c.get(0));
			OutilTest.assertEquals("Coureur invalide a la place 2 en ordre croissant",j2.getCoureur(Coureur.SPRINTEUR), c.get(1));
			OutilTest.assertEquals("Coureur invalide a la place 3 en ordre croissant",j1.getCoureur(Coureur.ROULEUR), c.get(2));
			OutilTest.assertEquals("Coureur invalide a la place 4 en ordre croissant",j1.getCoureur(Coureur.SPRINTEUR), c.get(3));

			c = jeu.getCoureursOrdonnes(true);
			OutilTest.assertEquals("Coureur invalide a la place 1 en ordre décroissant",j2.getCoureur(Coureur.ROULEUR), c.get(3));
			OutilTest.assertEquals("Coureur invalide a la place 2 en ordre décroissant",j2.getCoureur(Coureur.SPRINTEUR), c.get(2));
			OutilTest.assertEquals("Coureur invalide a la place 3 en ordre décroissant",j1.getCoureur(Coureur.ROULEUR), c.get(1));
			OutilTest.assertEquals("Coureur invalide a la place 4 en ordre décroissant",j1.getCoureur(Coureur.SPRINTEUR), c.get(0));
		} catch(Exception e)
		{
			OutilTest.fail("Exception: "+e);
		}
	}
	
	public static void test_c_estCase_estPlace()
	{
		Jeu jeu = new Jeu();
		Joueur j1 = null, j2 = null;
		try{
			j1 = jeu.ajouterJoueur("j1", "rouge");
			j2 = jeu.ajouterJoueur("j2", "vert");
			j1.getCoureur(Coureur.ROULEUR).setInitialPos(0, 0);
			j1.getCoureur(Coureur.SPRINTEUR).setInitialPos(0, 1);
			j2.getCoureur(Coureur.SPRINTEUR).setInitialPos(2, 0);
			j2.getCoureur(Coureur.ROULEUR).setInitialPos(3, 1);
			
			OutilTest.assertEquals("La case 0 devrait etre pleine",true, jeu.estCasePleine(0, null));
			OutilTest.assertEquals("La case 2 ne devrait pas etre pleine",false, jeu.estCasePleine(2, null));
			OutilTest.assertEquals("La case 1 ne devrait pas etre pleine",false, jeu.estCasePleine(1, null));
			OutilTest.assertEquals("La case 0 ne devrait pas etre pleine",false, jeu.estCasePleine(0, j1.getCoureur(Coureur.ROULEUR)));
			
			OutilTest.assertEquals("La case 1 devrait etre vide",true, jeu.estCaseVide(1, null));
			OutilTest.assertEquals("La case 2 ne devrait pas etre vide",false, jeu.estCaseVide(2, null));
			OutilTest.assertEquals("La case 0 ne devrait pas etre vide",false, jeu.estCaseVide(0, null));
			OutilTest.assertEquals("La case 3 devrait etre vide",true, jeu.estCaseVide(3, j2.getCoureur(Coureur.ROULEUR)));
			
			OutilTest.assertEquals("La place 3,1 devrait etre pleine",true, jeu.estPlacePleine(3,1, null));
			OutilTest.assertEquals("La place 2,1 ne devrait pas etre pleine",false, jeu.estPlacePleine(2,1, null));
			OutilTest.assertEquals("La place 1,0 ne devrait pas etre pleine",false, jeu.estPlacePleine(1,0, null));
			OutilTest.assertEquals("La place 0,0 ne devrait pas etre pleine",false, jeu.estPlacePleine(0, 0, j1.getCoureur(Coureur.ROULEUR)));
		} catch(ValeurIncorrecteException e)
		{
			OutilTest.fail("Exception: "+e);
		} catch(CouleurUtiliseeException e)
		{
			OutilTest.fail("Exception: "+e);
		}
	}
	
	public static void test_d_getCoureurAPosition()
	{
		Jeu jeu = new Jeu();
		Joueur j1 = null, j2 = null;
		try{
			j1 = jeu.ajouterJoueur("j1", "rouge");
			j2 = jeu.ajouterJoueur("j2", "vert");
			j1.getCoureur(Coureur.ROULEUR).setInitialPos(0, 0);
			j1.getCoureur(Coureur.SPRINTEUR).setInitialPos(0, 1);
			j2.getCoureur(Coureur.SPRINTEUR).setInitialPos(2, 0);
			j2.getCoureur(Coureur.ROULEUR).setInitialPos(3, 1);
		
			OutilTest.assertEquals("Mauvais coureur 1",j1.getCoureur(Coureur.ROULEUR), jeu.getCoureurAPosition(0,0, null));
			OutilTest.assertEquals("Mauvais coureur 2",j1.getCoureur(Coureur.SPRINTEUR), jeu.getCoureurAPosition(0,1, null));
			OutilTest.assertEquals("Mauvais coureur 3",j2.getCoureur(Coureur.SPRINTEUR), jeu.getCoureurAPosition(2,0, null));
			OutilTest.assertEquals("Mauvais coureur 4",j2.getCoureur(Coureur.ROULEUR), jeu.getCoureurAPosition(3,1,null));
			
			OutilTest.assertEquals("Il n'y a pas de coureur sur la place 2,1",true, jeu.getCoureurAPosition(2,1, null) == null);
			OutilTest.assertEquals("Il n'y a pas de coureur sur la place 4,0",true, jeu.getCoureurAPosition(4,0, null) == null);
			OutilTest.assertEquals("Il n'y a pas de coureur sur la place 0,0",true, jeu.getCoureurAPosition(0,0, j1.getCoureur(Coureur.ROULEUR)) == null);
			
			} catch(ValeurIncorrecteException e)
		{
			OutilTest.fail("Exception: "+e);
		} catch(CouleurUtiliseeException e)
		{
			OutilTest.fail("Exception: "+e);
		}
	}
	
	
	public static void test_f_getGagnant()
	{
		Jeu jeu = new Jeu();
		Joueur j1 = null, j2 = null;
		try{
			jeu.loadEtape("Avenue Corso Paseo");
			j1 = jeu.ajouterJoueur("j1", "rouge");
			j2 = jeu.ajouterJoueur("j2", "vert");
			j1.getCoureur(Coureur.ROULEUR).setInitialPos(0, 0);
			OutilTest.assertEquals("Pas de gagnant",true, jeu.getGagnant(jeu.getCoureurs()) == null);
			j1.getCoureur(Coureur.SPRINTEUR).setInitialPos(73, 0);
			OutilTest.assertEquals("Mauvais gagnant",j1.getCoureur(Coureur.SPRINTEUR), jeu.getGagnant(jeu.getCoureurs()));
			j2.getCoureur(Coureur.SPRINTEUR).setInitialPos(73, 1);
			OutilTest.assertEquals("Mauvais gagnant",j1.getCoureur(Coureur.SPRINTEUR), jeu.getGagnant(jeu.getCoureurs()));
			j2.getCoureur(Coureur.ROULEUR).setInitialPos(75, 1);
			OutilTest.assertEquals("Mauvais gagnant",j2.getCoureur(Coureur.ROULEUR), jeu.getGagnant(jeu.getCoureurs()));
		
			} catch(ValeurIncorrecteException e)
		{
			OutilTest.fail("Exception: "+e);
		} catch(CouleurUtiliseeException e)
		{
			OutilTest.fail("Exception: "+e);
		} catch(FichierEtapeException e)
		{
			OutilTest.fail("Exception: "+e);
		} catch(IOException e)
		{
			OutilTest.fail("Exception: "+e);
		}
	}
	
	public void test_g_grouper()
	{
		Jeu jeu = new Jeu();
		Joueur j1 = null, j2 = null, j3 = null, j4 = null;
		try{
			jeu.loadEtape("Avenue Corso Paseo");
			j1 = jeu.ajouterJoueur("j1", "rouge");
			j2 = jeu.ajouterJoueur("j2", "vert");
			j3 = jeu.ajouterJoueur("j3", "noir");
			j4 = jeu.ajouterJoueur("j4", "bleu");
			
			j1.getCoureur(Coureur.ROULEUR).setInitialPos(0, 0);
			j1.getCoureur(Coureur.SPRINTEUR).setInitialPos(1, 1);
			j2.getCoureur(Coureur.SPRINTEUR).setInitialPos(1, 0);
			
			j2.getCoureur(Coureur.ROULEUR).setInitialPos(3, 0);
			j3.getCoureur(Coureur.SPRINTEUR).setInitialPos(4, 0);
			
			j3.getCoureur(Coureur.ROULEUR).setInitialPos(7, 0);
			
			j4.getCoureur(Coureur.SPRINTEUR).setInitialPos(9, 0);
			j4.getCoureur(Coureur.ROULEUR).setInitialPos(10, 0);
			
			List<Groupe> g = jeu.grouper();
			OutilTest.assertEquals("Mauvais nombre de groupes",4, g.size());
			
			Groupe g1 = g.get(0);
			OutilTest.assertEquals("Groupe 1: mauvais nombre de coureurs", 3, g1.getTaille());
			OutilTest.assertEquals("Groupe 1: mauvais coureur 1", j2.getCoureur(Coureur.SPRINTEUR), g1.get(0));
			OutilTest.assertEquals("Groupe 1: mauvais coureur 2", j1.getCoureur(Coureur.SPRINTEUR), g1.get(1));
			OutilTest.assertEquals("Groupe 1: mauvais coureur 3", j1.getCoureur(Coureur.ROULEUR), g1.get(2));

			Groupe g2 = g.get(1);
			OutilTest.assertEquals("Groupe 2: mauvais nombre de coureurs", 2, g2.getTaille());
			OutilTest.assertEquals("Groupe 2: mauvais coureur 1", j3.getCoureur(Coureur.SPRINTEUR), g2.get(0));
			OutilTest.assertEquals("Groupe 2: mauvais coureur 2", j2.getCoureur(Coureur.ROULEUR), g2.get(1));
			
			Groupe g3 = g.get(2);
			OutilTest.assertEquals("Groupe 3: mauvais nombre de coureurs", 1, g3.getTaille());
			OutilTest.assertEquals("Groupe 3: mauvais coureur 1", j3.getCoureur(Coureur.ROULEUR), g3.get(0));
			
			Groupe g4 = g.get(3);
			OutilTest.assertEquals("Groupe 4: mauvais nombre de coureurs", 2, g4.getTaille());
			OutilTest.assertEquals("Groupe 4: mauvais coureur 1", j4.getCoureur(Coureur.ROULEUR), g4.get(0));
			OutilTest.assertEquals("Groupe 4: mauvais coureur 2", j4.getCoureur(Coureur.SPRINTEUR), g4.get(1));
			} catch(ValeurIncorrecteException e)
		{
			OutilTest.fail("Exception: "+e);
		} catch(CouleurUtiliseeException e)
		{
			OutilTest.fail("Exception: "+e);
		} catch(FichierEtapeException e)
		{
			OutilTest.fail("Exception: "+e);
		} catch(IOException e)
		{
			OutilTest.fail("Exception: "+e);
		}
	}
	
	public void test_h_aspiration()
	{
		Jeu jeu = new Jeu();
		Joueur j1 = null, j2 = null, j3 = null, j4 = null;
		try{
			jeu.loadEtape("Avenue Corso Paseo");
			j1 = jeu.ajouterJoueur("j1", "rouge");
			j2 = jeu.ajouterJoueur("j2", "vert");
			j3 = jeu.ajouterJoueur("j3", "noir");
			j4 = jeu.ajouterJoueur("j4", "bleu");
			
			j1.getCoureur(Coureur.ROULEUR).setInitialPos(0, 0);
			j1.getCoureur(Coureur.SPRINTEUR).setInitialPos(1, 1);
			j2.getCoureur(Coureur.SPRINTEUR).setInitialPos(1, 0);
		
			j2.getCoureur(Coureur.ROULEUR).setInitialPos(3, 0);
			j3.getCoureur(Coureur.SPRINTEUR).setInitialPos(4, 0);
			
			j3.getCoureur(Coureur.ROULEUR).setInitialPos(6, 0);
			
			j4.getCoureur(Coureur.SPRINTEUR).setInitialPos(9, 0);
			j4.getCoureur(Coureur.ROULEUR).setInitialPos(10, 0);
			
			List<Groupe> g = jeu.grouper();
			jeu.aspiration(g);
			
			OutilTest.assertEquals("Il devrait y avoir 2 groupes", 2, g.size());
			Groupe g1 = g.get(0);
			OutilTest.assertEquals("Groupe 1 : Mauvais nombre de coureur", 6, g1.getTaille());
			OutilTest.assertEquals("Groupe 1 : Mauvais coureur 1", j3.getCoureur(Coureur.ROULEUR), g1.get(0));
			OutilTest.assertEquals("Groupe 1 : Mauvais coureur 2", j3.getCoureur(Coureur.SPRINTEUR), g1.get(1));
			OutilTest.assertEquals("Groupe 1 : Mauvais coureur 3", j2.getCoureur(Coureur.ROULEUR), g1.get(2));
			OutilTest.assertEquals("Groupe 1 : Mauvais coureur 4", j2.getCoureur(Coureur.SPRINTEUR), g1.get(3));
			OutilTest.assertEquals("Groupe 1 : Mauvais coureur 5", j1.getCoureur(Coureur.SPRINTEUR), g1.get(4));
			OutilTest.assertEquals("Groupe 1 : Mauvais coureur 6", j1.getCoureur(Coureur.ROULEUR), g1.get(5));
			Groupe g2 = g.get(1);
			OutilTest.assertEquals("Groupe 2 : Mauvais nombre de coureur", 2, g2.getTaille());
			OutilTest.assertEquals("Groupe 2 : Mauvais coureur 1", j4.getCoureur(Coureur.ROULEUR), g2.get(0));
			OutilTest.assertEquals("Groupe 2 : Mauvais coureur 2", j4.getCoureur(Coureur.SPRINTEUR), g2.get(1));
			
			OutilTest.assertEquals("Mauvaise position coureur 1", 2, j1.getCoureur(Coureur.ROULEUR).getPos());
			OutilTest.assertEquals("Coureur 1 : La file ne devrait pas changer", 0, j1.getCoureur(Coureur.ROULEUR).getFile());

			OutilTest.assertEquals("Mauvaise position coureur 2", 3, j1.getCoureur(Coureur.SPRINTEUR).getPos());
			OutilTest.assertEquals("Coureur 2 : La file ne devrait pas changer", 1, j1.getCoureur(Coureur.SPRINTEUR).getFile());
			
			OutilTest.assertEquals("Mauvaise position coureur 3", 3, j2.getCoureur(Coureur.SPRINTEUR).getPos());
			OutilTest.assertEquals("Coureur 3 : La file ne devrait pas changer", 0,  j2.getCoureur(Coureur.SPRINTEUR).getFile());
			
			OutilTest.assertEquals("Mauvaise position coureur 4", 4, j2.getCoureur(Coureur.ROULEUR).getPos());
			OutilTest.assertEquals("Coureur 4 : La file ne devrait pas changer", 0, j2.getCoureur(Coureur.ROULEUR).getFile());
			
			OutilTest.assertEquals("Mauvaise position coureur 5", 5, j3.getCoureur(Coureur.SPRINTEUR).getPos());
			OutilTest.assertEquals("Coureur 5 : La file ne devrait pas changer", 0, j3.getCoureur(Coureur.SPRINTEUR).getFile());
			
			OutilTest.assertEquals("Mauvaise position coureur 6", 6, j3.getCoureur(Coureur.ROULEUR).getPos());
			OutilTest.assertEquals("Coureur 6 : La file ne devrait pas changer", 0, j3.getCoureur(Coureur.ROULEUR).getFile());
			
			OutilTest.assertEquals("Mauvaise position coureur 7", 9, j4.getCoureur(Coureur.SPRINTEUR).getPos());
			OutilTest.assertEquals("Coureur 7 : La file ne devrait pas changer", 0, j4.getCoureur(Coureur.SPRINTEUR).getFile());
			
			OutilTest.assertEquals("Mauvaise position coureur 8", 10, j4.getCoureur(Coureur.ROULEUR).getPos());
			OutilTest.assertEquals("Coureur 8 : La file ne devrait pas changer", 0, j4.getCoureur(Coureur.ROULEUR).getFile());
			} catch(ValeurIncorrecteException e)
		{
			OutilTest.fail("Exception: "+e);
		} catch(CouleurUtiliseeException e)
		{
			OutilTest.fail("Exception: "+e);
		} catch(FichierEtapeException e)
		{
			OutilTest.fail("Exception: "+e);
		} catch(IOException e)
		{
			OutilTest.fail("Exception: "+e);
		}
	}
	
	public void test_i_aspiration_montee()
	{
		Jeu jeu = new Jeu();
		Joueur j1 = null, j2 = null, j3 = null, j4 = null;
		try{
			//Carte 5s 17n 4m 4d 16n 7m 3d 17n 5a
			jeu.loadEtape("Firenze - Milano");
			j1 = jeu.ajouterJoueur("j1", "rouge");
			j2 = jeu.ajouterJoueur("j2", "vert");
			j3 = jeu.ajouterJoueur("j3", "noir");
			j4 = jeu.ajouterJoueur("j4", "bleu");
			
			j1.getCoureur(Coureur.ROULEUR).setInitialPos(18, 0);
			
			j1.getCoureur(Coureur.SPRINTEUR).setInitialPos(20, 0);
			
			j2.getCoureur(Coureur.SPRINTEUR).setInitialPos(22, 0);
			j2.getCoureur(Coureur.ROULEUR).setInitialPos(23, 0);

			j3.getCoureur(Coureur.ROULEUR).setInitialPos(25, 1);
			j3.getCoureur(Coureur.SPRINTEUR).setInitialPos(25, 0);
			
			j4.getCoureur(Coureur.SPRINTEUR).setInitialPos(27, 0);
			
			j4.getCoureur(Coureur.ROULEUR).setInitialPos(29, 0);
			
			List<Groupe> g = jeu.grouper();
			jeu.aspiration(g);
			
			OutilTest.assertEquals("Il devrait y avoir 4 groupes", 4, g.size());
			Groupe g1 = g.get(0);
			OutilTest.assertEquals("Groupe 1 : Mauvais nombre de coureur", 2, g1.getTaille());
			OutilTest.assertEquals("Groupe 1 : Mauvais coureur 1", j1.getCoureur(Coureur.SPRINTEUR), g1.get(0));
			OutilTest.assertEquals("Groupe 1 : Mauvais coureur 2", j1.getCoureur(Coureur.ROULEUR), g1.get(1));
			Groupe g2 = g.get(1);
			OutilTest.assertEquals("Groupe 2 : Mauvais nombre de coureur", 2, g2.getTaille());
			OutilTest.assertEquals("Groupe 2 : Mauvais coureur 1", j2.getCoureur(Coureur.ROULEUR), g2.get(0));
			OutilTest.assertEquals("Groupe 2 : Mauvais coureur 2", j2.getCoureur(Coureur.SPRINTEUR), g2.get(1));
			Groupe g3 = g.get(2);
			OutilTest.assertEquals("Groupe 3 : Mauvais nombre de coureur", 2, g3.getTaille());
			OutilTest.assertEquals("Groupe 3 : Mauvais coureur 1", j3.getCoureur(Coureur.SPRINTEUR), g3.get(0));
			OutilTest.assertEquals("Groupe 3 : Mauvais coureur 2", j3.getCoureur(Coureur.ROULEUR), g3.get(1));
			Groupe g4 = g.get(3);
			OutilTest.assertEquals("Groupe 4 : Mauvais nombre de coureur", 2, g4.getTaille());
			OutilTest.assertEquals("Groupe 4 : Mauvais coureur 1", j4.getCoureur(Coureur.ROULEUR), g4.get(0));
			OutilTest.assertEquals("Groupe 4 : Mauvais coureur 2", j4.getCoureur(Coureur.SPRINTEUR), g4.get(1));
			
			OutilTest.assertEquals("Coureur 1 devrait etre aspire 1 fois", 19, j1.getCoureur(Coureur.ROULEUR).getPos());
			OutilTest.assertEquals("Coureur 1 : La file ne devrait pas changer", 0, j1.getCoureur(Coureur.ROULEUR).getFile());

			OutilTest.assertEquals("Coureur 2 ne devrait pas etre aspire", 20, j1.getCoureur(Coureur.SPRINTEUR).getPos());
			OutilTest.assertEquals("Coureur 2 : La file ne devrait pas changer", 0, j1.getCoureur(Coureur.ROULEUR).getFile());
			
			OutilTest.assertEquals("Coureur 3 ne devrait pas etre aspire", 22, j2.getCoureur(Coureur.SPRINTEUR).getPos());
			OutilTest.assertEquals("Coureur 3 : La file ne devrait pas changer", 0, j1.getCoureur(Coureur.ROULEUR).getFile());
			
			OutilTest.assertEquals("Coureur 4 ne devrait pas etre aspire", 23, j2.getCoureur(Coureur.ROULEUR).getPos());
			OutilTest.assertEquals("Coureur 4 : La file ne devrait pas changer", 0, j1.getCoureur(Coureur.ROULEUR).getFile());
			
			OutilTest.assertEquals("Coureur 5 ne devrait pas etre aspire", 25, j3.getCoureur(Coureur.SPRINTEUR).getPos());
			OutilTest.assertEquals("Coureur 5 : La file ne devrait pas changer", 0, j1.getCoureur(Coureur.ROULEUR).getFile());
			
			OutilTest.assertEquals("Coureur 6 ne devrait pas etre aspire", 25, j3.getCoureur(Coureur.ROULEUR).getPos());
			OutilTest.assertEquals("Coureur 6 : La file ne devrait pas changer", 0, j1.getCoureur(Coureur.ROULEUR).getFile());
			
			OutilTest.assertEquals("Coureur 7 devrait etre aspire", 28, j4.getCoureur(Coureur.SPRINTEUR).getPos());
			OutilTest.assertEquals("Coureur 7 : La file ne devrait pas changer", 0, j1.getCoureur(Coureur.ROULEUR).getFile());
			
			OutilTest.assertEquals("Mauvaise position coureur 8", 29, j4.getCoureur(Coureur.ROULEUR).getPos());
			OutilTest.assertEquals("Coureur 8 : La file ne devrait pas changer", 0, j1.getCoureur(Coureur.ROULEUR).getFile());
			} catch(ValeurIncorrecteException e)
		{
			OutilTest.fail("Exception: "+e);
		} catch(CouleurUtiliseeException e)
		{
			OutilTest.fail("Exception: "+e);
		} catch(FichierEtapeException e)
		{
			OutilTest.fail("Exception: "+e);
		} catch(IOException e)
		{
			OutilTest.fail("Exception: "+e);
		}
	}
	
	public void test_j_fatigue()
	{
		Jeu jeu = new Jeu();
		Joueur j1 = null, j2 = null, j3 = null, j4 = null;
		try{
			//Carte 5s 17n 4m 4d 16n 7m 3d 17n 5a
			jeu.loadEtape("Firenze - Milano");
			j1 = jeu.ajouterJoueur("j1", "rouge");
			j2 = jeu.ajouterJoueur("j2", "vert");
			j3 = jeu.ajouterJoueur("j3", "noir");
			j4 = jeu.ajouterJoueur("j4", "bleu");
			Joueur[] joueurs = {j1, j2, j3, j4};
			int[] types = {Coureur.SPRINTEUR, Coureur.ROULEUR};
			int nbPioche = 3;
			
			for (Joueur j : joueurs)
				for (int t: types)
					for (int i = 0; i < nbPioche; i++)
						j.piocherCartes(t);
			
			j1.getCoureur(Coureur.SPRINTEUR).setInitialPos(20, 0);
			j1.getCoureur(Coureur.ROULEUR).setInitialPos(19, 0);
			j2.getCoureur(Coureur.SPRINTEUR).setInitialPos(21, 0);
			
			j2.getCoureur(Coureur.ROULEUR).setInitialPos(24, 0);
			j3.getCoureur(Coureur.SPRINTEUR).setInitialPos(25, 0);
			j3.getCoureur(Coureur.ROULEUR).setInitialPos(25, 1);
			
			j4.getCoureur(Coureur.SPRINTEUR).setInitialPos(28, 0);
			j4.getCoureur(Coureur.ROULEUR).setInitialPos(29, 0);
			
			List<Groupe> g = jeu.grouper();
			jeu.fatigue(g);
			
			boolean excepted[]= {false, false, true, false, true, true, false, true};
			int i = 0;
			for (Joueur j : joueurs)
			{
				for (int t: types)
				{
					Coureur c = j.getCoureur(t);
					List<Carte> cartes = j.piocherCartes(t);
					boolean cond = cartes.size() == 4 && cartes.get(3) instanceof CarteFatigue;
					OutilTest.assertEquals("Coureur "+c+(excepted[i] ? " devrait" : " ne devrait pas")+" etre fatigue", excepted[i], cond);
					i++;
				}
			}
		} catch(ValeurIncorrecteException e)
		{
			OutilTest.fail("Exception: "+e);
		} catch(CouleurUtiliseeException e)
		{
			OutilTest.fail("Exception: "+e);
		} catch(FichierEtapeException e)
		{
			OutilTest.fail("Exception: "+e);
		} catch(IOException e)
		{
			OutilTest.fail("Exception: "+e);
		}
	}
	
	
	public static void main(String[] args) {
		Lanceur.lancer(new TestJeu(), args);
	}

}
