import libtest.Lanceur;
import libtest.OutilTest;

import java.io.IOException;
import java.util.List;

/**
 * Une classe de test sur les Groupe.
 * @author Adrien
 * @author Arthur
 *
 */
public class TestGroupe {
	
	public void test_a_ajouter()
	{
		Groupe g = new Groupe();
		Coureur c1 = new Sprinteur(null);
		Coureur c2 = new Sprinteur(null);
		Coureur c3 = new Rouleur(null);
		Coureur c4 = new Rouleur(null);

		try{
			c1.setInitialPos(1, 1);
			c2.setInitialPos(1, 0);
			c3.setInitialPos(3, 0);
			c4.setInitialPos(5, 0);
		}catch(Exception e) {
			OutilTest.fail(e.toString());
		}
		
		g.ajouter(c4);
		g.ajouter(c1);
		g.ajouter(c3);
		g.ajouter(c2);
		
		OutilTest.assertEquals("Le coureur devrait etre c4", c4, g.get(0));
		OutilTest.assertEquals("Le coureur devrait etre c3", c3, g.get(1));
		OutilTest.assertEquals("Le coureur devrait etre c2", c2, g.get(2));
		OutilTest.assertEquals("Le coureur devrait etre c1", c1, g.get(3));
	}
	
	public void test_b_remove()
	{
		Groupe g = new Groupe();
		Coureur c1 = new Sprinteur(null);
		Coureur c2 = new Sprinteur(null);
		Coureur c3 = new Rouleur(null);
		g.ajouter(c1);
		g.ajouter(c2);
		g.remove(1);
		OutilTest.assertEquals("Le coureur devrait etre c1", c1, g.get(0));
		g.ajouter(c3);
		g.remove(0);
		OutilTest.assertEquals("Le coureur devrait etre c3", c3, g.get(0));
		
	}
	
	public void test_c_getTaille()
	{
		Groupe g = new Groupe();
		Coureur c1 = new Sprinteur(null);
		Coureur c2 = new Sprinteur(null);
		Coureur c3 = new Rouleur(null);
		g.ajouter(c1);
		g.ajouter(c2);
		g.remove(1);
		OutilTest.assertEquals("La taille est erronee", 1, g.getTaille());
		g.ajouter(c3);
		OutilTest.assertEquals("La taille est erronee", 2, g.getTaille());
		g.remove(0);
		g.remove(0);
		OutilTest.assertEquals("La taille est erronee", 0, g.getTaille());
	}
	
	public void test_d_getFirst()
	{
		Groupe g = new Groupe();
		Coureur c1 = new Sprinteur(null);
		Coureur c2 = new Sprinteur(null);
		Coureur c3 = new Rouleur(null);
		Coureur c4 = new Rouleur(null);
		Coureur c5 = new Rouleur(null);
		try{
			c1.setInitialPos(1, 1);
			c2.setInitialPos(2, 1);
			c3.setInitialPos(3, 0);
			c4.setInitialPos(5, 0);
			c5.setInitialPos(5, 1);
		}catch(Exception e) {
			OutilTest.fail(e.toString());
		}
		g.ajouter(c1);
		g.ajouter(c2);
		g.ajouter(c3);
		g.ajouter(c4);
		g.ajouter(c5);
		OutilTest.assertEquals("Le premier coureur est errone", c4, g.getFirst());
		
	}
	
	public void test_e_getLast()
	{
		Groupe g = new Groupe();
		Coureur c1 = new Sprinteur(null);
		Coureur c2 = new Sprinteur(null);
		Coureur c3 = new Rouleur(null);
		Coureur c4 = new Rouleur(null);
		Coureur c5 = new Rouleur(null);
		try{
			c1.setInitialPos(1, 0);
			c2.setInitialPos(2, 1);
			c3.setInitialPos(3, 0);
			c4.setInitialPos(5, 0);
			c5.setInitialPos(5, 1);
		}catch(Exception e) {
			OutilTest.fail(e.toString());
		}
		g.ajouter(c1);
		g.ajouter(c2);
		g.ajouter(c3);
		g.ajouter(c4);
		g.ajouter(c5);
		OutilTest.assertEquals("Le dernier coureur est errone", c1, g.getLast());
		
	}
	
	public void test_f_pasDeCoureurs()
	{
		Groupe g = new Groupe();		
		OutilTest.assertEquals("-1 devrait etre retourne", null, g.getLast());
		OutilTest.assertEquals("-1 devrait etre retourne", null, g.getFirst());
		OutilTest.assertEquals("null devrait etre retourne", true, g.getCoureursTete() == null);
	}
	
	public void test_g_getCoureursTeteDouble()
	{
		Groupe g = new Groupe();
		Coureur c1 = new Sprinteur(null);
		Coureur c2 = new Sprinteur(null);
		Coureur c3 = new Rouleur(null);
		Coureur c4 = new Rouleur(null);
		Coureur c5 = new Rouleur(null);
		try{
			c1.setInitialPos(1, 0);
			c2.setInitialPos(2, 1);
			c3.setInitialPos(3, 0);
			c4.setInitialPos(5, 0);
			c5.setInitialPos(5, 1);
		}catch(Exception e) {
			OutilTest.fail(e.toString());
		}
		g.ajouter(c1);
		g.ajouter(c2);
		g.ajouter(c3);
		g.ajouter(c4);
		g.ajouter(c5);
		List<Coureur> tete = g.getCoureursTete();
		OutilTest.assertEquals("Le nombre de coureurs en tete est errone", 2, tete.size());
		OutilTest.assertEquals("Le premier coureur est errone", c4, tete.get(0));
		OutilTest.assertEquals("Le deuxime coureur est errone", c5, tete.get(1));
		
	}
	
	public void test_h_getCoureursTeteSimple()
	{
		Groupe g = new Groupe();
		Coureur c1 = new Sprinteur(null);
		Coureur c2 = new Sprinteur(null);
		Coureur c3 = new Rouleur(null);
		Coureur c4 = new Rouleur(null);
		Coureur c5 = new Rouleur(null);
		try{
			c1.setInitialPos(1, 0);
			c2.setInitialPos(2, 1);
			c3.setInitialPos(3, 0);
			c4.setInitialPos(5, 0);
			c5.setInitialPos(6, 0);
		}catch(Exception e) {
			OutilTest.fail(e.toString());
		}
		g.ajouter(c1);
		g.ajouter(c2);
		g.ajouter(c3);
		g.ajouter(c4);
		g.ajouter(c5);
		List<Coureur> tete = g.getCoureursTete();
		OutilTest.assertEquals("Le nombre de coureurs en tete est errone", 1, tete.size());
		OutilTest.assertEquals("Le premier coureur est errone", c5, tete.get(0));
		
	}
	
	public void test_i_getCoureurs1Coureur()
	{
		Groupe g = new Groupe();
		Coureur c1 = new Sprinteur(null);
		try{
			c1.setInitialPos(1, 0);
		}catch(Exception e) {
			OutilTest.fail(e.toString());
		}
		g.ajouter(c1);
		List<Coureur> tete = g.getCoureursTete();
		OutilTest.assertEquals("Le nombre de coureurs en tete est errone", 1, tete.size());
		OutilTest.assertEquals("Le premier coureur est errone", c1, tete.get(0));
	}
	
	public void test_j_avancer()
	{
		Jeu jeu = new Jeu();
		try{
			// 22sn 4m
			jeu.loadEtape("Firenze - Milano");
			
			Coureur c1 = new Sprinteur(null);
			Coureur c2 = new Sprinteur(null);
			Coureur c3 = new Rouleur(null);
			Coureur c4 = new Rouleur(null);
			Coureur c5 = new Rouleur(null);
			Coureur c6 = new Rouleur(null);
			Coureur c7 = new Rouleur(null);
			
			c1.setInitialPos(1, 0);
			c2.setInitialPos(1, 1);
			
			c3.setInitialPos(23, 0);
			c4.setInitialPos(24, 0);
			c5.setInitialPos(24, 1);
			
			c6.setInitialPos(25, 0);
			c7.setInitialPos(26, 0);
			
			Groupe g1 = new Groupe(), g2 = new Groupe(), g3 = new Groupe();
		
			g1.ajouter(c1); g1.ajouter(c2);
			g2.ajouter(c3); g2.ajouter(c4); g2.ajouter(c5);
			g3.ajouter(c6); g3.ajouter(c7);
			
			OutilTest.assertEquals("Groupe 1 : Mauvais nombre de coureurs deplaces", 2, g1.avancer(jeu));
			OutilTest.assertEquals("Groupe 2 : Mauvais nombre de coureurs deplaces", 0, g2.avancer(jeu));
			OutilTest.assertEquals("Groupe 3 : Mauvais nombre de coureurs deplaces", 1, g3.avancer(jeu));

			OutilTest.assertEquals("Groupe 1 : Mauvais position du coureur 1", 2, c1.getPos());
			OutilTest.assertEquals("Groupe 1 : Mauvais file du coureur 1", 0, c1.getFile());
			OutilTest.assertEquals("Groupe 1 : Mauvais position du coureur 2", 2, c2.getPos());
			OutilTest.assertEquals("Groupe 1 : Mauvais file du coureur 2", 1, c2.getFile());
			
			OutilTest.assertEquals("Groupe 2 : Mauvais position du coureur 1", 23, c3.getPos());
			OutilTest.assertEquals("Groupe 2 : Mauvais file du coureur 1", 0, c3.getFile());
			OutilTest.assertEquals("Groupe 2 : Mauvais position du coureur 2", 24, c4.getPos());
			OutilTest.assertEquals("Groupe 2 : Mauvais file du coureur 2", 0, c4.getFile());			
			OutilTest.assertEquals("Groupe 2 : Mauvais position du coureur 3", 24, c5.getPos());			
			OutilTest.assertEquals("Groupe 2 : Mauvais file du coureur 3", 1, c5.getFile());

			OutilTest.assertEquals("Groupe 3 : Mauvais position du coureur 1", 25, c6.getPos());			
			OutilTest.assertEquals("Groupe 3 : Mauvais file du f coureur 1", 0, c6.getFile());
			OutilTest.assertEquals("Groupe 3 : Mauvais position du coureur 2", 27, c7.getPos());			
			OutilTest.assertEquals("Groupe 3 : Mauvais file du coureur 2", 0, c7.getFile());
			
		} catch(ValeurIncorrecteException e)
		{
			OutilTest.fail("Exception: "+e);
		} catch(FichierEtapeException e)
		{
			OutilTest.fail("Exception: "+e);
		} catch(IOException e)
		{
			OutilTest.fail("Exception: "+e);
		}
	}
	
	
	
	public static void main(String[] args) {
		Lanceur.lancer(new TestGroupe(), args);
	}

}
