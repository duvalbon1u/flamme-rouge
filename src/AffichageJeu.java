import javax.swing.JPanel;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
* Classe permettant l'affichage du plateau de jeu
* @author Adrien
*/
@SuppressWarnings("serial") //Not used here
class AffichageJeu extends JPanel {
	/**
	 * Jeu à afficher
	 */
	private Jeu jeu;
	/**
	 * Grande police
	 */
	private Font grand;
	/**
	 * Petite police
	 */
	private Font petit;
	/**
	 * Pour convertir des noms de couleurs en couleurs rgb
	 */
	private HashMap<String, Color> couleurToRGB;
	
	/**
	 * Constructeur de l'affichage à partir d'un Jeu j
	 * @param j le Jeu qui va être afficher
	 */
	public AffichageJeu(Jeu j)
	{
		jeu = j;
		grand = new Font("Verdana", Font.PLAIN, 36);
		petit = new Font("Verdana", Font.PLAIN, 12);
		couleurToRGB = new HashMap<String, Color>();
		couleurToRGB.put(Couleur.ROUGE.toString(), Color.RED);
		couleurToRGB.put(Couleur.VERT.toString(), new Color(50,180,50));
		couleurToRGB.put(Couleur.BLEU.toString(), Color.BLUE);
		couleurToRGB.put(Couleur.NOIR.toString(), Color.BLACK);
	}
	
	@Override
	public void paintComponent(Graphics g)
	{
		setBackground(Color.WHITE);
		super.paintComponent(g);
		
		Etape e = jeu.getEtape();
		
		boolean fin = false;
		final int caseW = 25, fileH = 26, caseH = 2 * (fileH + 5);
		final int w = getWidth();
		final int nbCases = w / caseW;
		
		g.setFont(grand);
		
		String tourStr = "Tour "+jeu.getTour();
		FontMetrics fMGrand = g.getFontMetrics();
		int textW = fMGrand.stringWidth(tourStr);
		g.drawString(tourStr, w / 2 - textW / 2, getHeight() - 10);

		g.setFont(petit);
		FontMetrics fMPetit = g.getFontMetrics();
		
		List<Joueur> joueurs = jeu.getJoueurs();
		List<Coureur> coureurs = jeu.getCoureursOrdonnes(false);
		for (int i = 0; i < joueurs.size(); i++)
		{
			Joueur j = joueurs.get(i);
			g.setColor(couleurToRGB.get(j.getCouleur().toString()));
			
			String strJoueur = j.toString();
			textW = fMPetit.stringWidth(strJoueur);
			
			int x = getWidth() / 4 + (getWidth() / 2) * (i%2) - textW/2, y = getHeight() - 80 * (2 - i/2);
		
			g.drawString(strJoueur, x, y);
				
			int c, pos;
			List<Couleur> couleurs = new ArrayList<Couleur>();
			for (c = 0, pos = 0; !coureurs.get(c).getJoueur().getCouleur().equals(j.getCouleur()) ; c++)
			{
				Couleur cou = coureurs.get(c).getJoueur().getCouleur();
				if (!couleurs.contains(cou))
				{
					pos++;
					couleurs.add(cou);
				}
			}
			g.drawString((pos+1)+(pos == 0 ? "ère" : "ème")+" place", x, y + 18);
		}
			
		
		for (int i = 0; !fin; i++)
		{
			int tuileType = Tuile.NORMAL;
			try {
				tuileType = e.getTuileType(i);
			} catch (IndexOutOfBoundsException ex)
			{
				fin = true;
				continue;
			}
			for (int f = 0; f < 2; f++)
			{
				int x = (i % nbCases) * caseW, y =(1 - f)*fileH + (i/nbCases) * caseH;
				g.setColor(Color.BLACK);
				switch (tuileType)
				{
					case Tuile.DEPART:
					case Tuile.ARRIVE:
						g.setColor(Color.lightGray);
						g.fillRect(x, y, caseW, fileH);
						g.setColor(Color.BLACK);
						break;
					case Tuile.MONTE:
						g.setColor(Color.RED);
						break;
					case Tuile.DESCENTE:
						g.setColor(Color.BLUE);
						break;						
				}
				if (!jeu.estPlacePleine(i, f, null))
				{
					if (tuileType == Tuile.MONTE)
						g.drawString("  >", x, y + fileH - 8);
					else if (tuileType == Tuile.DESCENTE)	
						g.drawString("  >", x, y + fileH - 8);
				}
				g.drawRect(x, y, caseW, fileH);
				
			}
		}
		for (Coureur c : jeu.getCoureurs())
		{
			g.setColor(couleurToRGB.get(c.getJoueur().getCouleur().toString()));
			int f = c.getFile();
			int x = (c.getPos() % nbCases) * caseW + 2, y =(1 - f)*fileH + (c.getPos()/nbCases) * caseH + 2;
			g.drawRect(x, y, caseW - 5 , fileH - 5);
			g.drawString(""+c.toString().toUpperCase().charAt(0),x + 7, y + fileH - 10);
		}
	}
}
