import java.io.*;
import java.util.*;

/**
 * Classe principale du jeu, qui gère l'intialisation et les différentes phases.
 * Son point d'entrée est la méthode {@link #run()}.
 * @author Adrien
 * @author Arthur
 */
public class Jeu {
	/**
	 * Liste des joueurs.
	 */
	private List<Joueur> joueurs;
	/**
	 * Étape sur laquelle on joue.
	 */
	private Etape etape;
	/**
	 * Tour de jeu.
	 */
	private int tour;

	/**
	 * Constructeur vide de Jeu
	 */
	public Jeu()
	{
		tour = 1;
		etape = null;
		joueurs = new ArrayList<Joueur>();
	}
	/**
	 * Getter pour l'attribut {@link #etape}.
	 * @return L'étape du jeu.
	 */
	public Etape getEtape()
	{
		return etape;
	}
	
	/**
	 * Getter pour l'attribut {@link #tour}.
	 * @return Le tour de jeu actuel.
	 */
	public int getTour()
	{
		return tour;
	}
	
	/**
	 * Getter pour l'attribut {@link #joueurs}
	 * @return La liste des joueurs.
	 */
	public List<Joueur> getJoueurs()
	{
		return joueurs;
	}
	
	/**
	 * Méthode qui ajoute un joueur en évitant que deux joueurs aient la même couleur.
	 * @param nom Le nom du nouveau Joueur.
	 * @param couleur La Couleur du nouveau Joueur
	 * @return L'adresse du nouveau Joueur
	 * @throws ValeurIncorrecteException Si jamais la couleur est invalide 
	 * @throws CouleurUtiliseeException Si jamais la couleur est déjà utilisée
	 */
	public Joueur ajouterJoueur(String nom, String couleur) throws ValeurIncorrecteException, CouleurUtiliseeException
	{
		Couleur c = Couleur.createCouleur(couleur); //Throws ValeurIncorrecteException
		for (Joueur j : joueurs)
		{
			if (j.getCouleur().equals(c))
				throw new CouleurUtiliseeException();
		}
		Joueur j = new Joueur(c,nom);
		joueurs.add(j);
		return j;		
	}
	
	/**
	 * Retourne tous les coureurs de la partie
	 * @return Une liste de tous les coureurs.
	 */
	public List<Coureur> getCoureurs()
	{
		List<Coureur> ret = new ArrayList<Coureur>();
		try {
			for (Joueur j : joueurs)
			{
				ret.add(j.getCoureur(Coureur.ROULEUR));
				ret.add(j.getCoureur(Coureur.SPRINTEUR));
			}
		} catch(ValeurIncorrecteException e)
		{
			System.err.println("Type de coureur invalide dans getCoureurs().");
		}
		return ret;
	}
	
	/**
	 * Retourne une liste ordonnée de tous les coureurs du jeu.
	 * @param croissant Vrai si on veut une liste dans l'ordre croissant de position, faux si décroissant
	 * @return Une liste ordonnée de coureurs
	 */
	public List<Coureur> getCoureursOrdonnes(boolean croissant)
	{
		List<Coureur> coureurs = getCoureurs();
		coureurs.sort(null);
		if (!croissant)
			Collections.reverse(coureurs);
		return coureurs;
	}
	
	/**
	 * Méthode qui renvoit un boolean indiquant si une case donnée est pleine.
	 * @param c numéro de la case.
	 * @param toExclude Coureur à exclure du test
	 * @return True si la case est complétement pleine et false si elle ne l'est pas.
	 */
	public boolean estCasePleine(int c, Coureur toExclude)
	{
		List<Coureur> coureurs = getCoureurs();
		int nb = 0;
		for (Coureur cour: coureurs)
		{
			if (cour != toExclude && cour.getPos() == c) {
				if (++nb == 2)
					return true;
			}
		}
		return false;
	}
	
	/**
	 * Méthode qui renvoit un boolean indiquant si une case donnée est vide.
	 * @param c numéro de la case.
	 * @param toExclude Coureur à exclure du test
	 * @return Vrai si la case est vide et false si elle ne l'est pas.
	 */
	public boolean estCaseVide(int c, Coureur toExclude)
	{
		List<Coureur> coureurs = getCoureurs();
		for (Coureur cour: coureurs)
		{
			if (cour != toExclude && cour.getPos() == c)
				return false;
		}
		return true;
	}
	
	/**
	 * Méthode qui renvoit un boolean indiquant si une place donnée est occupée.
	 * @param c numéro de la case.
	 * @param f file ({@link Tuile#FILE_DROITE} ou {@link Tuile#FILE_GAUCHE}).
	 * @param toExclude Coureur à exclure du test
	 * @return Vrai si la place est occupée et false si elle ne l'est pas.
	 */
	public boolean estPlacePleine(int c, int f, Coureur toExclude)
	{
		return getCoureurAPosition(c, f, toExclude) != null;
	}
	
	/**
	 * Permet de récuperer un coureur selon sa position.
	 * @param c numéro de la case
	 * @param f file ({@link Tuile#FILE_DROITE} ou {@link Tuile#FILE_GAUCHE}).
	 * @param toExclude Coureur à ignorer
	 * @return Coureur à la position donnée ou null si la place est vide
	 */
	public Coureur getCoureurAPosition(int c, int f, Coureur toExclude)
	{
		List<Coureur> coureurs = getCoureurs();
		for (Coureur cour: coureurs)
		{
			if (cour != toExclude && cour.getPos() == c && cour.getFile() == f)
				return cour;
		}
		return null;
	}
	
	/**
	 * Méthode qui renvoit le gagnant à partir d'une liste des coureurs
	 * @param coureurs une liste des coureurs du jeu
	 * @return Le coureur gagnant ou null si aucun coureur n'a dépassé la ligne d'arrivée.
	 */
	public Coureur getGagnant(List<Coureur> coureurs)
	{
		Coureur gagnant = null;
		for (Coureur c : coureurs)
		{
			int pos = c.getPos() - etape.getLigneArrivee();
			if (pos >= 0)
				if (gagnant == null || c.compareTo(gagnant) > 0)
					gagnant = c;
		}
		return gagnant;	
	}
	
	/**
	 * Méthode qui prend un Coureur et qui lui ajoute une carte fatigue s'il finit en montée
	 * @param c le coureur controlé
	 */
	public void fatigueMontee(Coureur c) {
		int typeCase = etape.getTuileType(c.getPos());
		if(typeCase == Tuile.MONTE) {
			try {
				c.getJoueur().ajouterFatigue(c.getType());
			} catch (ValeurIncorrecteException e) {
				System.err.println("Mauvais type de coureur");
			}
		}
	}
	
	/**
	 * Méthode qui initialise {@link #etape} à partir d'un nom d'étape
	 * @param nom de l'étape à charger
	 * @throws IOException Si l'étape n'existe pas ou que son fichier est inaccessible
	 * @throws FichierEtapeException Si le fichier de l'étape est corrompu
	 */
	public void loadEtape(String nom) throws IOException, FichierEtapeException
	{
		etape = new Etape(nom);
	}
	
	/**
	 * Permet de segmenter les coureurs en une liste de {@link Groupe}.
	 * @return une liste de groupes de coureurs
	 */
	public List<Groupe> grouper()
	{
		List<Coureur> coureurs = getCoureursOrdonnes(true);
		List<Groupe> groupes = new ArrayList<Groupe>();
		
		Groupe g = new Groupe();
		int lastCase = -1;
		for (Coureur c : coureurs)
		{
			if (lastCase != -1 && c.getPos() - lastCase >  1)
			{
				groupes.add(g);
				g = new Groupe();
			}	
			
			lastCase = c.getPos();
			g.ajouter(c);
		}
		if (g.getTaille() > 0)
			groupes.add(g);
		return groupes;
	}
	
	/**
	 * Méthode permettant de gérer la phase d'aspiration
	 * @param groupes Groupes formés par la fonction {@link #grouper()}
	 */
	public void aspiration(List<Groupe> groupes)
	{
		List<Groupe> toRemove = new ArrayList<Groupe>(); 
		for (int i = 0; i < groupes.size() - 1; i++)
		{
			Groupe devant = groupes.get(i+1);
			Groupe derriere = groupes.get(i);
			int posDernier = devant.getLast().getPos();
			if (etape.getTuileType(posDernier) != Tuile.MONTE && posDernier - derriere.getFirst().getPos() == 2)
			{
				int nbAspire = derriere.avancer(this);
				for (int k = 0; k < nbAspire; k++)
				{
					Coureur c = derriere.remove(0);
					devant.ajouter(c);
				}
				if (derriere.getTaille() == 0)
					toRemove.add(derriere);
			}
		}
		groupes.removeAll(toRemove);
	}
	
	/**
	 * Méthode permettant de gérer la phase de fatigue
	 * @param groupes Liste des groupes après la phase d'{@link #aspiration(java.util.List)}
	 */
	public void fatigue(List<Groupe> groupes)
	{
		for (Groupe gr : groupes)
		{
			List<Coureur> tete = gr.getCoureursTete();
			for (Coureur c : tete)
			{
				try{
					c.getJoueur().ajouterFatigue(c.getType());
				}catch(ValeurIncorrecteException e)
				{
					System.err.println("Erreur de type pour le coureur "+c);
				}
			}
		}
	}
	
	/**
	 * Point d'entrée de la classe Jeu. Initialise l'étape, puis les joueurs, et boucle sur les 3 phases de jeu jusqu'à la fin du jeu.
	 */
	public void run()
	{
		Interaction.presentation();
		Interaction.initAffichage(this);
		Interaction.demandeCreerEtape();
		
		while (etape == null)
		{
			String nom = Interaction.choisirEtape();
			try{
				loadEtape(nom);
			}
			catch(FileNotFoundException e)
			{
				System.out.println("Cette étape n'existe pas !");
			}
			catch(FichierEtapeException e)
			{
				System.out.println("Le fichier correspondant à cette étape est invalide.");
			}
			catch (IOException e)
			{
				System.out.println("Erreur à la lecture du fichier.");
			}
		}
		Interaction.afficherJeu(this);
		Interaction.initJoueurs(this);
		
		for (Joueur j : joueurs)
			Interaction.placerCoureurs(j, this);
		
		boolean fini = false;
		Coureur gagnant = null;
		while (!fini)
		{
			//Phase 1
			Interaction.afficherJeu(this);
			for (Joueur j : joueurs)
			{
				int type = -1;
				while (type < 0)
				{
					type = Interaction.typePremierPiocheChoix(j);
					try{
						int types[] = {type, type == Coureur.ROULEUR ? Coureur.SPRINTEUR : Coureur.ROULEUR};
						for (int i = 0; i < types.length; i++)
						{
							List<Carte> main = j.piocherCartes(types[i]);
							int choix = Interaction.piocherChoix(main, types[i]);
							j.choisirCarte(main, choix, types[i]);
						}
					}catch(ValeurIncorrecteException e)
					{
						System.out.println("Veuillez rentrer un type valide.");
					}
				}
			}
			//Phase 2
			List<Coureur> coureurs = getCoureursOrdonnes(false);
			for (Coureur c : coureurs)
			{
				Interaction.afficherJeu(this);
				try {
					c.avancer(this);
				} catch (PasDeCarteException e) {
					System.err.println("Pas de carte sélectionnée pour le coureur "+c);
				}
				try {
					Thread.sleep(2000);
				}catch(InterruptedException e){}
				//Carte fatigue en montée
				this.fatigueMontee(c);
			}
			gagnant = getGagnant(coureurs);
			if (gagnant != null)
				fini = true;
			//Phase 3
			List<Groupe> groupes = grouper();
			aspiration(groupes);
			fatigue(groupes);
			tour++;
		}
		Interaction.affichageVictoire(gagnant);
	}
}
