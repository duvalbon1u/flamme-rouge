@SuppressWarnings("serial") // Pas besoin ici
/**
 * Classe modélisant une exception dans le cas où un coureur doit se déplacer mais auquel le joueur n'a pas affecté de carte.
 * @author Adrien
 */
public class PasDeCarteException extends Exception {
	/**
	 * Constructeur par défaut
	 */
	public PasDeCarteException()
	{
		super();
	}
}
