import java.util.*;

/**
 * Classe permettant la gestion d'une pile de carte d'un joueur, comprenant pioche et défausse du rouleur et du sprinteur.
 * Gère les {@link Carte cartes énergie} et les {@link CarteFatigue cartes fatigues}.
 * @author Adrien
 */
public class PileJoueur {
	/**
	 * La pioche du sprinteur.
	 */
	private ArrayList<Carte> piocheSprinteur;
	/**
	 * La pioche du rouleur.
	 */
	private ArrayList<Carte> piocheRouleur;
	/**
	 * La défausse du sprinteur.
	 */
	private ArrayList<Carte> defausseSprinteur;
	/**
	 * La défausse du rouleur.
	 */
	private ArrayList<Carte> defausseRouleur;
	/**
	 * Les compositions initiales des pioches du rouleur et du sprinteur, le nombre représentant la valeur des cartes énergies.
	 */
	private final static int[] 	CARTES_SPRINTEUR= {2,2,2,3,3,3,4,4,4,5,5,5,9,9,9};
	/**
	 * Les compositions initiales des pioches du rouleur et du sprinteur, le nombre représentant la valeur des cartes énergies.
	 */
	private final static int[] 	CARTES_ROULEUR 	= {3,3,3,4,4,4,5,5,5,6,6,6,7,7,7};
	
	/**
	 * Constructeur de la classe PileJoueur, initialise des défausses vides et des pioches mélangées contenant toutes les cartes de leur type.
	 */
	public PileJoueur()
	{
		piocheSprinteur = new ArrayList<Carte>();
		defausseSprinteur = new ArrayList<Carte>();
		piocheRouleur = new ArrayList<Carte>();
		defausseRouleur = new ArrayList<Carte>();
		
		for (int s : CARTES_SPRINTEUR)
			piocheSprinteur.add(new Carte(s));
		for (int r : CARTES_ROULEUR)
			piocheRouleur.add(new Carte(r));
		
		melanger(piocheSprinteur, defausseSprinteur);
		melanger(piocheRouleur, defausseRouleur);
	}
	
	/**
	 * Permet de défausser une carte c pour le coureur de type spécifié, cette carte sera remise en jeu si la pioche est vide (voir la méthode {@link #piocher(int)}).
	 * @param c Carte a ajouter a la défausse
	 * @param type Type {@link Coureur#SPRINTEUR sprinteur} ou {@link Coureur#ROULEUR rouleur}
	 * @throws ValeurIncorrecteException Lève une ValeurIncorrectException si le type de coureur est invalide.
	 * @see #piocher(int)
	 * @see Carte
	 */
	public void defausser(Carte c, int type) throws ValeurIncorrecteException
	{
		if (type == Coureur.SPRINTEUR)
			defausseSprinteur.add(c);
		else if (type == Coureur.ROULEUR)
			defausseRouleur.add(c);
		else
			throw new ValeurIncorrecteException("Type de coureur incorrect.");
	}
	/**
	 * Permet de piocher une carte de la pioche correspondant au coureur du type spécifié.
	 * Si la pioche est vide, re-mélange la pioche avec la défausse avant de retourner la carte.
	 * @param type Type du coureur: {@link Coureur#SPRINTEUR sprinteur} ou {@link Coureur#ROULEUR rouleur}
	 * @return La carte piochée, ou null si aucune carte ne peut être piochée
	 * @throws ValeurIncorrecteException Lève une ValeurIncorrectException si le type de coureur est invalide.
	 * @see #defausser(Carte, int)
	 * @see Carte
	 */
	public Carte piocher(int type) throws ValeurIncorrecteException
	{
		ArrayList<Carte> pioche, defausse;
		if (type == Coureur.SPRINTEUR)
		{
			pioche = piocheSprinteur;
			defausse = defausseSprinteur;
		}
		else if (type == Coureur.ROULEUR)
		{
			pioche = piocheRouleur;
			defausse = defausseRouleur;
		}
		else
			throw new ValeurIncorrecteException("Type de coureur incorrect.");

		if (pioche.size() == 0)
		{
			if (defausse.size() == 0)
				return null;
			
			melanger(pioche, defausse);
		}	
		return pioche.remove(pioche.size() - 1);
	}
	
	/**
	 * Méthode privée de mélange de cartes, vide la défausse dans la pioche et la mélange.
	 * @param pioche La pioche a remplir
	 * @param defausse La défausse a vider
	 */
	private void melanger(ArrayList<Carte> pioche, ArrayList<Carte> defausse)
	{
		pioche.addAll(defausse);
		defausse.clear();
		Collections.shuffle(pioche);
	}
	
	/**
	* Méthode permettant d'appliquer le dopage en convertissant 1,2 ou 3 cartes des plus faibles en cartes plus élevées.
	* @param dose Nombre de doses de dopage à administrer
	* @throws ValeurIncorrecteException Lève une exception si le nombre de doses est trop élevé.
	*/
	public void appliquerDopage(int dose) throws ValeurIncorrecteException
	{
		if (dose < 0 || dose > 3)
			throw new ValeurIncorrecteException("Le nombre de doses est incorrect");
		for (int i = 0; i < dose; i++)
		{
			piocheRouleur.remove(0); //Premieres cartes = plus faibles
			piocheRouleur.add(new Carte(7));
			piocheSprinteur.remove(0);
			piocheSprinteur.add(new Carte(9));
		}
	}
}
