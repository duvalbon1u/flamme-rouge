/**
 * Classe représentant une exception où la contrainte d'unicité de la couleur est violée
 * @author Adrien
 */

@SuppressWarnings("serial")
public class CouleurUtiliseeException extends Exception {
	public CouleurUtiliseeException()
	{
		super();
	}
}
