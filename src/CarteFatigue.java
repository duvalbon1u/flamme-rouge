/**
 * Classe représentant une carte fatigue, qui est un carte énergie spéciale.
 * @author Adrien
 *
 */
public class CarteFatigue extends Carte {
	/**
	 * Construit une carte fatigue, correspondant à une carte énergie de valeur 2.
	 */
	public CarteFatigue()
	{
		super(2);
		type = "fatigue";
	}
}
