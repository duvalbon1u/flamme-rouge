/**
 * Classe créant un sprinteur.
 * @author Adrien
 *
 */
public class Sprinteur extends Coureur {
	/**
	 * Construit un coureur de type Sprinteur
	 */
	public Sprinteur(Joueur j)
	{
		super(SPRINTEUR, j);
	}
}
