import java.io.IOException;
/**
 * Classe principale, permettant d'instancier la classe Jeu
 * @author Adrien
 */
public class Principale {

	public static void main(String[] args) throws IOException, InterruptedException {
		new Jeu().run();
	}

}
