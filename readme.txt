﻿PROJET FLAMME ROUGE:
Contenu:
	Le dossier src contient toutes les sources
	Le dossier bin contient tous les fichiers .class
	Le dossier Map contient tous les fichiers d'étapes
	Le dossier doc contient le documentation du projet

Notes:
	Les fichiers sources sont encodés en UTF-8
	Le jeu utilise une interface graphique ainsi que la console pour l'interaction utilisateur
	Le dépôt Git est publique, à l'URL : 

Règles: 
	Règles du jeu (incluant les règles additionnelles)
	Si on termine le tour en montée on récolte une carte fatigue
	Au début du jeu, on peut choisir de se doper pour transformer des cartes de valeur faible en cartes de valeur élevée (variante de la règle de base avec les handicaps)
